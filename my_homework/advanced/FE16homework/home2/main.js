const books = [
	{
		author: 'Скотт Бэккер',
		name: 'Тьма, что приходит прежде',
		price: 70,
	},
	{
		author: 'Скотт Бэккер',
		name: 'Воин-пророк',
	},
	{
		name: 'Тысячекратная мысль',
		price: 70,
	},
	{
		author: 'Скотт Бэккер',
		name: 'Нечестивый Консульт',
		price: 70,
	},
	{
		author: 'Дарья Донцова',
		name: 'Детектив на диете',
		price: 40,
	},
	{
		author: 'Дарья Донцова',
		name: 'Дед Снегур и Морозочка',
	},
];

const Books = function (book) {
	this.booksRight = [];
	book.forEach(item => {
		try {
			if (item.author === undefined) {
				throw new Error(`wrong is author`);
			} else if (item.name === undefined) {
				throw new Error(`wrong is name`);
			} else if (item.price === undefined) {
				throw new Error(`wrong is price`);
			}
			this.booksRight.push(item);
		} catch (er) {
			console.error(`${er.message}`);
		}
	});
	console.log(this.booksRight);
};

Books.prototype.createBooks = function () {
	const ulBooks = document.querySelector('ul');
	this.booksRight.forEach(book => {
		const paramBook = document.createElement('li');
		paramBook.classList.add('field__book--card');
		const lineParamAutor = document.createElement('p');
		lineParamAutor.classList.add('author');
		const lineParamName = document.createElement('p');
		lineParamName.classList.add('name');
		const lineParamPrice = document.createElement('p');
		lineParamPrice.classList.add('price');

		lineParamAutor.innerText = `${book.author}`;
		lineParamName.innerText = `${book.name}`;
		lineParamPrice.innerText = `${book.price}`;

		paramBook.append(lineParamAutor);
		paramBook.append(lineParamName);
		paramBook.append(lineParamPrice);
		ulBooks.append(paramBook);
	});
};

const booky = new Books(books);
booky.createBooks();

// function Exeption (message) {
// 	this.message = message;
// }
