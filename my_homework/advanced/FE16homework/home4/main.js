BASE_URL = 'https://api.ipify.org/?format=json';
IP_URL = 'http://ip-api.com/json/';
FIELD = '?fields=country,countryCode,region,regionName,city,district';
let api;
const ul = document.querySelector('.list__data');
const button = document.querySelector('button');

button.addEventListener('click', function () {
	fetch(`${BASE_URL}`)
		.then(data => data.json())
		.then(data => {
			console.log(data.ip);
			api = data.ip;
		})
		.then(data =>
			fetch(IP_URL + api + FIELD)
				.then(data => data.json())
				.then(data => {
					ul.innerHTML = `
					<li class = "list__data--item">${data.countryCode}</li>
			<li class = "list__data--item">${data.country}</li>
			<li class = "list__data--item">${data.region}</li>
			<li class = "list__data--item">${data.regionName}</li>
			<li class = "list__data--item">${data.city}</li>
			<li class = "list__data--item">${data.district}</li>
				`;
				})
		);
});

// async function asyncData({ BASE_URL, IP_URL, FIELD }) {
// 	// console.log({BASE_URL});
// 	const response = await fetch(BASE_URL);
// 	const { ip } = await response.json();
// 	console.log('IP:', ip);
// const res = await fetch(IP_URL + ip + FIELD);
// 	// const { continent, country, regionName, city, district } = await res.json();
// 	const result = await res.json();
// 	console.log('Adress:', result);
// }

// const delay = ms => {
// 	return new Promise(r => setTimeout(() => r(), ms));
// };

// class Ip {
// 	constructor() {
// 		this._ui();
// 	}

// 	_ui() {
// 		const button = document.querySelector('.ip');
// 		const fieldToData = document.createElement('div');
// 		button.addEventListener('click', this.buttonClick.bind(this));
// 		button.append(fieldToData);
// 	}

// 	buttonClick() {}
// }

// const ip = new Ip(BASE_URL);

// function getData(BASE_URL, IP_URL, FIELD) {
// 	console.log('Started');
// 	// const responses =
// 	fetch(BASE_URL).then(response => {
// 		response.json();
// 		const { ip } = response.ip;
// 		console.log(ip);
// 	});

// 	// 	console.log('IP:', response);
// 	// const res = await fetch(IP_URL + ip + FIELD);
// 	// 	// const { continent, country, regionName, city, district } = await res.json();
// 	// 	const result = await res.json();
// 	// 	console.log('Adress:', result);

// 	// fetch('http://ip-api.com/', {
// 	// 	method: 'POST',
// 	// }).then(responce => responce.json());
// }

// getData(BASE_URL, IP_URL, FIELD);
// .then(data => {
// 	if (data.ok) {
// 		console.log('Data:', data);
// 	} else {
// 		throw new Error(data.status);
// 	}
// })
// .catch(e => console.error(e));

// const urls = {
// 	BASE_URL: 'https://api.ipify.org/?format=json',
// 	IP_URL: 'http://ip-api.com/json/',
// 	FIELD: '?fields=country,countryCode,region,regionName,city,district,query',
// };

// async function asyncData({ BASE_URL, IP_URL, FIELD }) {
// 	// console.log({BASE_URL});
// 	const response = await fetch(BASE_URL);
// 	const { ip } = await response.json();
// 	console.log('IP:', ip);
// const res = await fetch(IP_URL + ip + FIELD);
// 	// const { continent, country, regionName, city, district } = await res.json();
// 	const result = await res.json();
// 	console.log('Adress:', result);
// }
// asyncData(urls);

// const url = {
// 	baseUrl: 'https://api.ipify.org/?format=json',
// 	ipAdress: 'http://ip-api.com/json/',
// 	fields:
// 		'?fields=status,message,country,countryCode,region,regionName,city,district,timezone,query',
// };
// async function takeIp({ baseUrl, ipAdress, fields }) {
// 	const response = await fetch(baseUrl);
// 	const { ip } = await response.json();
// 	const fullAdress = await fetch(ipAdress + ip + fields);
// 	const result = await fullAdress.json();
// 	console.log(result);
// }
// takeIp(url);

// const ajax = new XMLHttpRequest();
// const xhr = new XMLHttpRequest();
// const list = document.querySelector('.list__data');

// ajax.open('GET', `${BASE_URL}`);
// ajax.responseType = 'json';
// ajax.send();
// ajax.onload = function () {
// 	if (ajax.status === 404) {
// 		alert('Data not found');
// 	} else {
// 		let { ip } = ajax.response;

// 		console.log(ip);
// 		console.log(ajax.response);

// 		list.innerHTML = `<li>${ip}</li>`;
// 		const button = document.querySelector('.ip');

// 		button.addEventListener('click', function (event) {
// 			xhr.open('GET', `${IP_URL}` + `${ip}` + `${FIELD}`, false);
// 			xhr.send();
// 			xhr.onload = function () {
// 				if (xhr.status !== 200) {
// 					console.log(`Error ${xhr.status}:${xhr.statusText}`);
// 				} else {
// 					console.log(JSON.parse(xhr.response));
// 					let res = JSON.parse(xhr.response);
// 					console.log(res);
// 				}
// 			};
// 		});
// 	}
// };
