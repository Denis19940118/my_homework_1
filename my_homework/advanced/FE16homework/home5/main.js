const urls = {
	BASE_URL: 'https://api.ipify.org/?format=json',
	IP_URL: 'http://ip-api.com/json/',
	FIELD: '?fields=country,countryCode,region,regionName,city',
};

const button = document.querySelector('button');
button.onclick = function itemData() {
	asyncData(urls);
};
// .addEventListener('click', itemData(urls));

// function itemData(url) {
// 	asyncData(url);
// }

async function asyncData({ BASE_URL, IP_URL, FIELD }) {
	console.log('data');
	const response = await fetch(BASE_URL);
	const { ip } = await response.json();
	console.log('IP:', ip);
	const res = await fetch(IP_URL + ip + FIELD);
	const result = await res.json();
	console.log('Adress:', result);
	for (let [key, value] of Object.entries(result)) {
		const li = document.createElement('li');
		li.classList.add('list__data--item');
		li.textContent = value;
		button.after(li);
	}
	// })
}

// asyncData(urls);
// function itemData(url) {
// 	asyncData(url);
// }
// const ul = document.querySelector('.list__data');

//======================================
// const urls = {
// 	BASE_URL: 'https://api.ipify.org/?format=json',
// 	IP_URL: 'http://ip-api.com/json/',
// 	FIELD: '?fields=country,countryCode,region,regionName,city',
// };
// const button = document.querySelector('button');
// button.addEventListener('click', () => asyncData(urls));
// async function asyncData({ BASE_URL, IP_URL, FIELD }) {
// 	console.log('data');
// 	const response = await fetch(BASE_URL);
// 	const { ip } = await response.json();
// 	console.log('IP:', ip);
// 	const res = await fetch(IP_URL + ip + FIELD);
// 	const result = await res.json();
// 	console.log('Adress:', result);
// 	for (let [key, value] of Object.entries(result)) {
// 		const li = document.createElement('li');
// 		li.classList.add('list__data--item');
// 		li.textContent = value;
// 		button.after(li);
// 	}
// 	// })
// }

///================================================
// const urls = {
// 	BASE_URL: 'https://api.ipify.org/?format=json',
// 	IP_URL: 'http://ip-api.com/json/',
// 	FIELD: '?fields=country,countryCode,region,regionName,city',
// };
// const button = document.querySelector('button');
// button.addEventListener('click', asyncData.bind(this, urls));
// async function asyncData({ BASE_URL, IP_URL, FIELD }) {
// 	console.log('data');
// 	const response = await fetch(BASE_URL);
// 	const { ip } = await response.json();
// 	console.log('IP:', ip);
// 	const res = await fetch(IP_URL + ip + FIELD);
// 	const result = await res.json();
// 	console.log('Adress:', result);
// 	for (let [key, value] of Object.entries(result)) {
// 		const li = document.createElement('li');
// 		li.classList.add('list__data--item');
// 		li.textContent = value;
// 		button.after(li);
// 	}
// 	// })
// }
