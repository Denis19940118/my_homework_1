const [...nameCity] = ["Kyiv", "Berlin", "Dubai", "Moscow", "Paris"];
const [firstCity,...anotherCity] = ["Kyiv", "Berlin", "Dubai", "Moscow", "Paris"];
console.log(...nameCity);
console.log(firstCity,...anotherCity);

let employee = {
            name: "Manve",
            salary: 10000,
};

let {name, salary} = employee;
console.log(name, salary);

const array = ['value', 'showValue'];
const [value, showValue] = array;

alert(value); // будет выведено 'value'
alert(showValue);  // будет выведено 'showValue'