let project_folder = 'dist';
let sourse_folder = '#src';

let path = {
	build: {
		html: project_folder + '/',
		css: project_folder + '/css/',
		js: project_folder + '/js/',
		img: project_folder + '/assets/',
	},
	src: {
		html: sourse_folder + '/index.html',
		css: sourse_folder + '/scss/style.scss',
		js: sourse_folder + '/js/main.js',
		img: sourse_folder + '/assets/**/*.{jpg,png,svg,gif,ico,webp}',
	},
	watch: {
		html: sourse_folder + '/**/*.html',
		css: sourse_folder + '/scss/**/*.scss',
		js: sourse_folder + '/js/**/*.js',
		img: sourse_folder + '/assets/**/*.{jpg,png,svg,gif,ico,webp},',
	},
	clean: './' + project_folder + '/',
};

let { src, dist, dest } = require('gulp'),
	gulp = require('gulp'),
	browsersync = require('browser-sync').create();

function browserSync(param) {
	browsersync.init({
		server: {
			baseDir: './' + project_folder + '/',
		},
		port: 3000,
		notify: false,
	});
}

function html() {
	return src(path.src.html)
		.pipe(dest(path.build.html))
		.pipe(browsersync.stream());
}

let build = gulp.series(html);
let watch = gulp.parallel(build, browserSync);

exports.html = html;
exports.build = build;
exports.watch = watch;
exports.default = watch;
