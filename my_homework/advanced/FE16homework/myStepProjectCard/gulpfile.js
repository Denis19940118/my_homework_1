let project_folder = 'dist';
let sourse_folder = '#src';

let path = {
	build: {
		html: project_folder + '/',
		css: project_folder + '/css/',
		js: project_folder + '/js/',
		img: project_folder + '/assets/',
	},
	src: {
		html: [
			sourse_folder + '/html/*.html',
			'!' + sourse_folder + '/html/_*.html',
		],
		css: sourse_folder + '/scss/style.scss',
		js: sourse_folder + '/js/**/*.js',
		img: sourse_folder + '/assets/**/*.{jpg,png,svg,gif,ico,webp}',
	},
	watch: {
		html: sourse_folder + '/**/*.html',
		css: sourse_folder + '/scss/**/*.scss',
		js: sourse_folder + '/js/**/*.js',
		img: sourse_folder + '/assets/**/*.{jpg,png,svg,gif,ico,webp},',
	},
	clean: './' + project_folder + '/',
};

let { src, dest } = require('gulp'),
	gulp = require('gulp'),
	browsersync = require('browser-sync').create(),
	fileinclude = require('gulp-file-include'),
	del = require('del'),
	scss = require('gulp-sass'),
	autoprefexer = require('gulp-autoprefixer'),
	group_media = require('gulp-group-css-media-queries'),
	clean_css = require('gulp-clean-css'),
	gulp_rename = require('gulp-rename'),
	// uglify = require('gulp-uglify-es'),
	imagemin = require('gulp-imagemin'),
	webp = require('gulp-webp'),
	webphtml = require('gulp-webp-html');
// webpcss = require('gulp-webpcss');

function browserSync(param) {
	browsersync.init({
		server: {
			baseDir: './' + project_folder + '/',
		},
		port: 3000,
		notify: false,
	});
}

function html() {
	return src(path.src.html)
		.pipe(fileinclude())
		.pipe(webphtml())
		.pipe(dest(path.build.html))
		.pipe(browsersync.stream());
}

function css() {
	return src(path.src.css)
		.pipe(scss({ outputStyle: 'expanded' }))
		.pipe(group_media())
		.pipe(
			autoprefexer({
				overrideBrowserslist: ['last 5 versions'],
				cascade: true,
			})
		)

		.pipe(dest(path.build.css))
		.pipe(clean_css())
		.pipe(
			gulp_rename({
				extname: '.min.css',
			})
		)
		.pipe(dest(path.build.css))
		.pipe(browsersync.stream());
}

function js() {
	return (
		src(path.src.js)
			.pipe(fileinclude())
			.pipe(dest(path.build.js))
			// .pipe(uglify())
			// .pipe(
			// 	gulp_rename({
			// 		extname: '.min.js',
			// 	})
			// )
			// .pipe(dest(path.build.js))
			.pipe(browsersync.stream())
	);
}

function images() {
	return src(path.src.img)
		.pipe(
			webp({
				quality: 70,
			})
		)
		.pipe(dest(path.build.img))
		.pipe(src(path.src.img))
		.pipe(
			imagemin({
				progressive: true,
				cvgoPlugins: [{ removeViewBox: false }],
				interlaced: true,
				optimizationLevel: 3, //0 to 7
			})
		)
		.pipe(dest(path.build.img))
		.pipe(browsersync.stream());
}

function watchFIles(params) {
	gulp.watch([path.watch.html], html);
	gulp.watch([path.watch.css], css);
	gulp.watch([path.watch.js], js);
	gulp.watch([path.watch.img], images);
}

function clean(params) {
	return del(path.clean);
}

let build = gulp.series(clean, gulp.parallel(js, css, html, images));
let watch = gulp.parallel(build, watchFIles, browserSync);

exports.images = images;
exports.js = js;
exports.css = css;
exports.html = html;
exports.build = build;
exports.watch = watch;
exports.default = watch;

// const gulp = require('gulp'),
// 	sass = require('gulp-sass'),
// 	autoprefixer = require('gulp-autoprefixer'),
// 	cleanCSS = require('gulp-clean-css'),
// 	cleaner = require('gulp-clean'),
// 	concat = require('gulp-concat'),
// 	minify = require('gulp-js-minify'),
// 	uglify = require('gulp-uglify'),
// 	pipeline = require('readable-stream').pipeline,
// 	imagemin = require('gulp-imagemin'),
// 	browserSync = require('browser-sync').create();
// const path = {
// 	dist: {
// 		html: 'dist',
// 		css: 'dist/css',
// 		js: 'dist/js',
// 		img: 'dist/img',
// 		self: 'dist',
// 	},
// 	src: {
// 		html: 'src/html/*.html',
// 		scss: 'src/scss/**/*.*',
// 		js: 'src/js/*.js',
// 		img: 'src/img/**/**/*.*',
// 	},
// };
// /**************** F U N C T I O N S ***************/
// const htmlBuild = () =>
// 	gulp
// 		.src(path.src.html)
// 		.pipe(gulp.dest(path.dist.html))
// 		.pipe(browserSync.stream());
// const scssBuild = () =>
// 	gulp
// 		.src(path.src.scss)
// 		.pipe(sass().on('error', sass.logError))
// 		.pipe(autoprefixer(['> 0.01%', 'last 100 versions']))
// 		.pipe(gulp.dest(path.dist.css))
// 		.pipe(browserSync.stream());
// const imgBuild = () =>
// 	gulp
// 		.src(path.src.img)
// 		.pipe(imagemin())
// 		.pipe(gulp.dest(path.dist.img))
// 		.pipe(browserSync.stream());
// const jsBuild = () =>
// 	gulp
// 		.src(path.src.js)
// 		.pipe(concat('script.js'))
// 		.pipe(minify())
// 		.pipe(uglify())
// 		.pipe(gulp.dest(path.dist.js))
// 		.pipe(browserSync.stream());
// const cleanProd = () =>
// 	gulp
// 		.src(path.dist.self, { allowEmpty: true })
// 		.pipe(cleaner())
// 		.pipe(cleanCSS({ compatibility: 'ie8' }))
// 		.pipe(browserSync.stream());
// const icoBuild = () =>
// 	gulp
// 		.src(path.src.ico)
// 		.pipe(gulp.dest(path.dist.ico))
// 		.pipe(browserSync.stream());
// /****************** W A T C H E R ***************/
// const watcher = () => {
// 	browserSync.init({
// 		server: {
// 			baseDir: './dist',
// 		},
// 	});
// 	gulp.watch(path.src.html, htmlBuild).on('change', browserSync.reload);
// 	gulp.watch(path.src.scss, scssBuild).on('change', browserSync.reload);
// 	gulp.watch(path.src.js, jsBuild).on('change', browserSync.reload);
// 	gulp.watch(path.src.img, imgBuild).on('change', browserSync.reload);
// };
// /**************** T A S K S ****************/
// gulp.task(
// 	'default',
// 	gulp.series(
// 		cleanProd,
// 		htmlBuild,
// 		scssBuild,
// 		imgBuild,
// 		jsBuild,
// 		icoBuild,
// 		watcher
// 	)
// );
