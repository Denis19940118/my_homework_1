const draggableElements = document.querySelectorAll('.draggable');
const droppableElements = document.querySelectorAll('.droppable');

draggableElements.forEach(elem => {
	elem.addEventListener('dragstart', dragStart);
	// elem.addEventListener('drag', drag);
	// elem.addEventListener('dragend', dragEnd);
});

droppableElements.forEach(elem => {
	elem.addEventListener('dragenter', dragEnter);
	elem.addEventListener('dragover', dragOver);
	elem.addEventListener('dragleave', dragLeave);
	elem.addEventListener('drop', drop);
});

// Dragvand Drop Functions

function dragStart(event) {
	event.dataTransfer.setData('text', event.target.id);
}

function dragEnter(event) {
	if (!event.target.classList.contains('dropped')) {
		event.target.classList.add('droppable-hover');
	}
}

function dragOver(event) {
	if (!event.target.classList.contains('dropped')) {
		event.preventDefault();
	}
}

function dragLeave(event) {
	if (!event.target.classList.contains('dropped')) {
		event.target.classList.remove('droppable-hover');
	}
}

function drop(event) {
	event.preventDefault();
	event.target.classList.remove('.droppable-hover');
	console.log(13);
	const draggableElementData = event.dataTransfer.getData('text');
	const droppableElementData = event.target.getAttribute('data-draggable-id');
	event.target.style.backgroundColor = draggableElementData;
	if (draggableElementData === droppableElementData) {
		event.target.classList.add('dropped');
		const draggableElement = document.getElementById(draggableElementData);
		event.target.style.backgroundColor = draggableElement.style.color;
		// event.target.style.backgroundColor = window.getComputedStyle(
		// 	droppableElement
		// ).color;
		draggableElement.classList.add('dragged');
		draggableElement.getAttribute('draggable', 'false');
		event.target.insertAdjacentHTML(
			'afterbegin',
			`<i class="fas fa-${droppableElementData}">`
		);
	}
}
