// const text = document.querySelector('.text');
// const containerImage = document.querySelector('.image-container');
// const container = document.querySelector('.container');
const textChange = document.querySelector('.text-change');
const image = document.querySelector('.image');

document.addEventListener('mousemove', onMoveText);

function onMoveText(event) {
	x = event.clientX;
	y = event.clientY;

	image.style.transform = `translate3d(${x}px, ${y}px, 0)`;

	textChange.style.setProperty('--x', x + 'px');
	textChange.style.setProperty('--y', y + 'px');
}
