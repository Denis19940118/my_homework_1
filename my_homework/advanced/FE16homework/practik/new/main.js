function Component() {
	const icon = document.querySelector('.fa-edit');
	const replace = document.getElementById('replace');

	const card = document.querySelector('.card');
	const input = document.createElement('input');
	const iconAccept = document.createElement('i');
	console.dir(replace);
	icon.addEventListener('click', () =>
		createInput(icon, replace, card, input, iconAccept)
	);
	iconAccept.addEventListener(
		'click',
		createReplace.bind(this, icon, replace, card, input, iconAccept)
	);
}

function createInput(icon, replace, card, input, iconAccept) {
	console.log('active');
	input.id = 'edit-text';
	input.classList.add('form-control');
	input.type = 'text';
	input.value = replace.textContent.trim();

	replace.remove();
	icon.remove();

	iconAccept.className = 'fa fa-check';
	card.append(input, iconAccept);
}

function createReplace(icon, replace, card, input, iconAccept) {
	replace.textContent = input.value;

	input.remove();
	iconAccept.remove();

	replace.append(icon);
	card.append(replace);
}

Component();
