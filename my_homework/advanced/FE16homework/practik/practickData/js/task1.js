/** ЗАДАЧА - 1
 * Реализовать страницу новостной ленты используя синтаксис async await
 *
 * Отправить GET запрос для получения постов на:
 * @link https://ajax.test-danit.com
 *
 * Получим массив всех постов.
 * Для размещения на странице постов создать класс Post, которые дублирует все свойства объектов в ответе.
 * Так же в этом классе в конструкторе:
 *  - должны объявляться DOM элементы и записываться в качестве свойств.
 *  - создать метод render('.parent-elem-class'), который будет принимать 1 аргумент - селектор,
 * для того чтобы найти родительский элемент, куда нужно будет вставить элемент.
 *  Ответственность метода render() - разместить элемент на странице.
 *
 * Нужно отобразить на странице все эти посты в виде карточек.
 */

const API = 'https://ajax.test-danit.com/api/json/';

class ListPost {
	constructor() {
		this.getPosts();
	}

	async getPosts() {
		const response = await fetch(`${API}/posts`);
		response.json().then(data => {
			data.forEach(item => {
				const post = new Post(item);
				post.render('#container__great');
			});
		});
	}
}

class Post {
	constructor({ id, userId, title, body }) {
		this.id = id;
		this.userId = userId;
		this.title = title;
		this.body = body;

		this.elements = {
			userId: document.createElement('p'),
			title: document.createElement('p'),
			body: document.createElement('p'),
		};

		this.icon = document.createElement('i');

		const { userId: name, title: head, body: text } = this.elements;
		name.classList.add('post__string');
		head.classList.add('post__string');
		text.classList.add('post__string');
	}

	render(selector) {
		const container = document.querySelector(selector);
		this._boxPost = document.createElement('div');
		const { userId, title, body } = this.elements;
		this.icon.className = 'fa fa-edit';
		this.input = document.createElement('input');
		this.iconAccept = document.createElement('i');

		this._boxPost.classList.add('post');
		this._boxPost.id = this.id;

		const btnComment = document.createElement('button');
		btnComment.innerText = 'See comments';
		btnComment.addEventListener('click', this.onSeeComments.bind(this));

		const btnIcon = document.createElement('button');
		btnIcon.append(this.icon);
		btnIcon.addEventListener('click', this.onIcon.bind(this));

		this.getAuthor().then(name => {
			userId.innerText = name;
		});

		title.innerText = this.title;
		body.innerText = this.body;

		this._boxPost.append(userId, title, body, btnComment, btnIcon);
		container.append(this._boxPost);
	}

	onIcon() {}

	async getComment() {
		const response = await fetch(`${API}/comments?postId=${this.userId}`);
		return response.json();
	}

	onSeeComments() {
		this.getComment().then(data => {
			data.forEach(item => {
				const comment = new Comment(item);
				if (comment.idCom(this.userId)) {
					console.log('yes');
					comment.render(`${this.id}`);
					// comment.render(`.post[data-id="${this.id}"]`);
				}
			});
		});
	}

	async getAuthor() {
		const author = await fetch(`${API}/users/${this.userId}`, {
			headers: {
				'Access-Control-Allow-Origin': '*',
			},
		});
		return author.json().then(data => data.name);
	}

	appendTo(parent) {
		parent.append(this._container);
	}
}

const list = new ListPost();

// async function getPosts() {
// 	const response = await fetch(`${API}/posts`);
// 	return response.json();
// }
// console.log('post: ', getPosts());

// getPosts().then(data => {
// 	data.forEach(item => {
// 		const post = new Post(item);
// 		post.render('#container__great');
// 	});
// });
