/** ЗАДАЧА - 2
 * Добавить к каждому посту кнопку "See comments"
 *
 * Для этого в классе Post добавить метод onSeeComments
 * Он должен быть вызван в обработчике клика по кнопке "See comments"
 * Внутри этого метода отправляется запрос на получение комментариев этого конкретного поста.
 * Для начала выводим полученные комментарии в консоль.
 *
 * Создаем класс Comment созданный по принципу класса Post из предыдущего задания.
 */

class Comment {
	constructor({ postId, name, email, body }) {
		this.commentId = postId;
		this.name = name;
		this.email = email;
		this.body = body;

		this.elements = {
			name: document.createElement('p'),
			email: document.createElement('p'),
			body: document.createElement('p'),
		};
		// this.idCom()
		// const { name, email, body } = this.elements;
	}

	render(selector) {
		const container = document.getElementById(selector);
		this._boxComment = document.createElement('div');

		const { name, email, body } = this.elements;
		name.innerText = this.name;
		email.innerText = this.email;
		body.innerText = this.body;

		this._boxComment.append(name, email, body);
		container.append(this._boxComment);
	}

	idCom(id) {
		if (id === this.commentId) {
			return id;
		}
	}
}
