/* ЗАДАЧА 1
 * Изучить документацию сервера по адресу:
 * @link - https://192.168.10.72:9003/
 * https://ajax.test-danit.com
 *
 * Отправить GET запрос на сервер для получения всех пользователей.
 *
 * Преобразовать данные ответа в JavaScript объекты карточек пользователей.
 *
 * Разместить на странице карточки пользователей, которые будут содержать следующую информацию:
 *  - имя пользователя
 *  - никнейм
 *  - эл.почта
 *  - телефон
 *  - сайт
 *  - кнопка "See posts"
 * */
const API = 'https://ajax.test-danit.com/api/json';
