const menu = document.getElementById('menu');
let close = document.querySelector( '.close-humburger');
let cotlet = document.querySelector('.cotlet');

menu.addEventListener('toggle', onClick);

function onClick(event) {
    if (event.type === 'toggle') {
        close.classList.remove('hidden');
        cotlet.classList.add('hidden');
    }
}