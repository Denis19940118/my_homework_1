s; // GET PUT POST DELETE
// PUT то заменна внутри обекта пользавотелем
// POST это создание в форме нового обьекта

// const xhr = new XMLHttpRequest();
// const ajax = new XMLHttpRequest();
//
// ajax.open('GET', 'https://jsonplaceholder.typicode.com/users');
// ajax.responseType = 'json';
// ajax.send();
// ajax.onload = function () {
//     if ( ajax.status !== 200 ) {
//          console.log(`Error ${ajax.status}:${ajax.statusText}`);
//     }
//     else {
//         // let {name} = ajax.response;
//         const table = document.getElementById('table');
//         table.innerHTML = ajax.response.map(item => `<li>${item.name}</li>`).join('');
//         console.log(ajax);
//         console.log(ajax.response);
//     }
// };

const xhr = new XMLHttpRequest();
const ajax = new XMLHttpRequest();
const table = document.getElementById('table');

ajax.open('GET', 'https://swapi.dev/api/films/');
ajax.responseType = 'json';
ajax.send();
ajax.onload = function () {
	// if(ajax.status <=300 && ajax.status >= 200){
	//     const data = JSON.parse(ajax.response);
	//     console.log(data);
	// }
	//
	if (ajax.status !== 200) {
		console.log(`Error ${ajax.status}:${ajax.statusText}`);
	} else if (ajax.status === 404) {
		alert('Data not found');
	} else {
		let { results: episodes } = ajax.response;

		console.log(episodes);
		console.log(ajax.response);
		// let id = 0;
		let dataAttribute = 0;
		table.innerHTML = episodes
			.map(
				item => `<li class="characters" >
<strong>Название эпизода :</strong> ${item.title} 
<strong>Номер эпизода:</strong> ${item.episode_id}
<strong>Содержание</strong> ${item.opening_crawl}
<button class="btn" data-number=${dataAttribute++}>Персонажи</button>
</li>`
			)
			.join('');
		const buttons = document.querySelectorAll('.btn');
		// buttons.forEach(item =>
		table.addEventListener('click', function (event) {
			const boxPersonName = document.createElement('strong');
			const boxPerson = document.createElement('ul');
			const listEpisode = document.getElementsByClassName('characters');
			const target = event.target.getAttribute('data-number');
			listEpisode[target].after(boxPerson);
			boxPersonName.innerText = `Список персонажей ${target}`;
			console.log(listEpisode[target]);

			episodes[target].characters.forEach(item => {
				// console.log(item.characters);

				// characters.forEach(url => {
				xhr.open('GET', `${item}`, false);
				xhr.send();
				xhr.onload = function () {
					if (xhr.status !== 200) {
						console.log(`Error ${xhr.status}:${xhr.statusText}`);
					} else {
						console.log(JSON.parse(xhr.response));
						let res = JSON.parse(xhr.response);
						const list = document.createElement('li');
						list.innerText = `${res.name}`;
						boxPerson.append(list);
					}
				};
				// })
			});
			//  boxPersonName.append(boxPerson);
			// table.appendChild(boxPersonName);
		});
		// );
	}
};
// ajax.onprogress
