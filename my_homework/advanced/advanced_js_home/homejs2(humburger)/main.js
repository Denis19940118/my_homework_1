/**
 * Класс, объекты которого описывают параметры гамбургера.
 *
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка

 * @throws {_HamburgerException}  При неправильном использовании
 */
const SIZE_SMALL = 'SIZE_SMALL';
const SIZE_LARGE = 'SIZE_LARGE';
const SIZES = {
    SIZE_SMALL: {
        name: 'small humburger ',
        price: 50,
        kcal: 20
    },
    SIZE_LARGE: {
        name: 'large humburger ',
        price: 100,
        kcal: 40
    },
};
const STUFFING_CHEESE = 'STUFFING_CHEESE';
const STUFFING_POTATO = 'STUFFING_POTATO';
const STUFFING_SALAD = 'STUFFING_SALAD';
const STUFFINGS = {
    STUFFING_CHEESE: {
        name: ' with stuffing cheese ',
        price: 10,
        kcal: 20
    },
    STUFFING_SALAD: {
        name: ' with stuffing salad ',
        price: 20,
        kcal: 5
    },
    STUFFING_POTATO: {
        name: ' with stuffing potato ',
        price: 15,
        kcal: 10
    },
};
const TOPPING_MAYO = 'TOPPING_MAYO';
const TOPPING_SPICE = 'TOPPING_SPICE';
const TOPPINGS = {
    TOPPING_MAYO: {
        name: ' and with topping mayo',
        price: 20,
        kcal: 5
    },
    TOPPING_SPICE: {
        name: ' and with topping spice',
        price: 15,
        kcal: 0
    },
};

class Hamburger {
    constructor(size, stuffing) {
        // this._size = size;
        // this._stuffing = stuffing;
        // this._topping = [];
        // this._errors();
        try {
            if (!size) {
                throw new HamburgerException ('Wrong size');
            } else if (!Object.keys(SIZES).includes(size)) {
                throw new HamburgerException (`Wrong size${size}`);
            }
            this._size = size;

            if (!stuffing) {
                throw new HamburgerException ('Wrong stuffing');
            } else if (!Object.keys(STUFFINGS).includes(stuffing)) {
                throw new HamburgerException (`Wrong size${stuffing}`);
            }
            this._stuffing = stuffing;
            this._topping = [];
        } catch (error) {
            return console.error(`${error.name}:${error.message}`);
        }
    };

    // _errors() {
    //     // try {
    //     //     if (!this._size) {
    //     //         throw new HamburgerException('Wrong size');
    //     //     } else if (!Object.keys(SIZES).includes(size)) {
    //     //         throw new HamburgerException('Error in use, enter name (Hamburger.SIZE_SMALL,' +
    //     //         ' Hamburger.SIZE_LARGE)');
    //     //     }
    //     //
    //     //     if (!this._stuffing) {
    //     //         throw new HamburgerException('Wrong size');
    //     //     } else if (!Object.keys(STUFFINGS).includes(stuffing)) {
    //     //         throw new HamburgerException('Error in use, enter name (Hamburger.SIZE_SMALL,' +
    //     //         ' Hamburger.SIZE_LARGE)');
    //     //     }
    //     // } catch (e) {
    //     //     console.error(`${e.name}:${e.message}`);
    //     // }
    // };

    addTopping(topping) {
        try {
            if (!topping) {
                throw new HamburgerException('Wrong size');
            } else if (!Object.keys(TOPPINGS).includes(topping)) {
                throw new HamburgerException (`Wrong size${topping}`);
            } else if (this._topping.includes(topping)) {
                throw new HamburgerException('this topping is duplicate');
            }
            return this._topping.push(topping);
        } catch (e) {
            console.error(`${e.name}:${e.message}`);
        }
    }

    removeTopping(topping) {
        try {
            let arr = this._topping;
            if (!arr.includes(topping)) {
                throw new HamburgerException('impassible this operation ')
            }
            arr = arr.filter(item => item !== topping);
            if (arr.includes(topping)) {
                console.log(arr);
                console.log(this._topping);
                throw new HamburgerException("Topping don\`t remove")
            }
            return arr;
        } catch (error) {
            console.error(`${error.name}:${error.message}`)
        }
    };

    getToppings() {
        return this._topping;
    };

    getSize() {
        return this._size;
    };

    getStuffing() {
        return this._stuffing;
        // let arrayStuffing = [];
        // arrayStuffing.push(this._stuffing);
        // arrayStuffing.filter(el => {
        //     // console.log(el);
        //     return el !== undefined;
        // });
        // return arrayStuffing
    };

    calculatePrice() {
        let res;
        let size = SIZES[this._size].price;
        let stuffing = STUFFINGS[this._stuffing].price;
        let toppi = this._topping.map((el) => TOPPINGS[el].price);
        toppi.push(size);
        toppi.push(stuffing);
        return res = toppi.reduce((sum, current) => sum + current, 0);

    };

    calculateCalories = function () {
        let size = SIZES[this._size].kcal;
        let stuffing = STUFFINGS[this._stuffing].kcal;
        let toppi = this._topping.map((el) => TOPPINGS[el].kcal);
        toppi.push(size, stuffing);
        return toppi.reduce((sum, current) => sum + current, 0);
    };
}

class HamburgerException {
    constructor(message) {
        this.message = message;
    }
}


let hamburger = new Hamburger(SIZE_SMALL, STUFFING_SALAD);
// добавка из майонеза
hamburger.addTopping(TOPPING_MAYO);
// спросим сколько там калорий
console.log("Calories: %f", hamburger.calculateCalories());
// сколько стоит
console.log("Price: %f", hamburger.calculatePrice());
// я тут передумал и решил добавить еще приправу
hamburger.addTopping(TOPPING_SPICE);
// А сколько теперь стоит?
console.log("Price with sauce: %f", hamburger.calculatePrice());
// Проверить, большой ли гамбургер?
console.log("Is hamburger large: %s", hamburger.getSize() === SIZE_LARGE); // -> false
// Убрать добавку
hamburger.removeTopping(TOPPING_SPICE);
console.log("Have %d toppings", hamburger.getToppings().length);