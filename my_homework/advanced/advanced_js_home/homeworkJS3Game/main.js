class Game {
    constructor() {
        this.render(document.body);
    }

    static get Level() {
        return {
            EASY: 1500,
            MIDDLE: 1000,
            HARD: 500
        }
    }

    render(container) {
        container.innerHTML = `
<div class="game">
    <div class="game__level">
        <button>
            <div class="btn level disable chosen" id="easy">EASY</div>
        </button>
        <button>
            <div class="btn level disable" id="middle">MIDDLE</div>
        </button>
        <button>
            <div class="btn level disable" id="hard">HARD</div>
        </button>
    </div>
    <button>
        <div class="btn disable" id="start">START</div>
    </button>
    <div class="div score__section--wrapper">
        <div class="score__block">
            <span class="score-title" id="score-title-person">Person</span>
            <span class="score-value" id="score-value-person"></span>
        </div>

        <div class="score__block">
            <span class="score-title" id="score-title-computer">Computer</span>
            <span class="score-value" id="score-value-computer"></span>
        </div>
    </div>
    <table class="field" id="field"></table>
</div>`;

        this.createFieldUI();
        this.changeLevel();

        this._userScore = document.getElementById('score-value-person');
        this._computerScore = document.getElementById('score-value-computer');
        this._userScore.innerText = '0';
        this._computerScore.innerText = '0';
        this.countCom = 1;
        this.countUse = 1;

        const btnStart = document.getElementById('start');
        btnStart.onclick = () => {
            this._timerId = null;
            this.winner = '';

            const cellArr = document.querySelectorAll('.cell');
            cellArr.forEach(el =>
                el.classList.remove('green-cell', 'red-cell', 'cell-active'));
            this._randomCell();
        }
    }

    createFieldUI() {
        const table = document.querySelector('.field');
        let tdCell = [];
        const emptyField = Array.from({length: 10}, (e, i) =>
            Array.from({length: 10}, (e, j) =>
                [i, j]));

        for (let i = 0; i < emptyField.length; i++) {
            const row = document.createElement('tr');
            for (let j = 0; j < emptyField.length; j++) {
                const cell = document.createElement('td');
                tdCell.push(cell);
                row.append(cell);
                cell.classList.add('cell');
            }
            table.append(row);
        }
        this._table = table;
    }

    changeLevel() {
        const buttons = document.querySelectorAll('.level');
        buttons.forEach(el => {
            el.addEventListener('click', ev => {
                buttons.forEach(el => el.classList.remove('chosen'));
                ev.target.classList.add('chosen');
                this.level = ev.target.textContent;
                console.log(this.level)
            })
        })
    }

    itemButton() {
            const level = document.querySelector('.chosen');
            return Game.Level[level.textContent];
    }

    _randomCell() {
        const listCell = document.querySelectorAll('.cell');
        // const listCellArr = Array.from(listCell);
        // const cellIndex = Math.floor((listCellArr.length) * Math.random());
        // const currentCell = listCellArr[cellIndex];
        // listCellArr[cellIndex];
        const shuffledEmptyField = [...listCell].sort(() => Math.random() - 0.5);
        this.shuffledEmptyField = shuffledEmptyField;
        console.log(shuffledEmptyField);
        // const currentCell = shuffledEmptyField.pop();
        // currentCell.classList.add('cell-active');
        // this._cellIndex = currentCell;
        this._activeCell();
    }

    _activeCell() {
        if(this.shuffledEmptyField.length === 0){
            console.log(this.shuffledEmptyField.length);
            this._winner();
        }
        const currentCell = this.shuffledEmptyField.pop();
        currentCell.classList.add('cell-active');
        this._activeCells = currentCell;
        console.log(this._activeCell);
        this._nextStep();
    }

    _nextStep() {
        this.levelTime = this.itemButton();
        this._timerId = setTimeout(() => {
            // if(this._cellIndex.includes('green-cell')){
            //     this._userScored();
            // }
            this._computerScored();
            // this._nextStep();
            this._listenClick();
            this._activeCell();
        }, this.levelTime);
    }

    _computerScored() {
        this._activeCells.classList.remove('cell-active');
        this._activeCells.classList.add('red-cell');
        const count = this.countCom++;
        this._computerScore.innerText = count;

    }

    _userScored() {
        this._activeCells.classList.remove('cell-active');
        this._activeCells.classList.add('green-cell');
        const count = this.countUse++;
        this._userScore.innerText = count;
    }

    _onClick(event) {
        if (event.target === this._activeCells) {
            console.log(this._activeCells);
            clearTimeout(this._timerId);
            this._userScored();
            // this._nextStep();
            this._listenClick();
            this._activeCell();
        }
    }

    _listenClick() {
        this._clickedCell = this._onClick.bind(this);
        this._table.addEventListener('click', this._clickedCell);
    }

    _winner() {
        if(this.countUse > this.countCom) {
            // alert(`${document.getElementById('score-title-person').value} is win`);
             return alert(`person is win`);
        }
        // alert(`${document.getElementById('score-title-computer').value} is win`);
        alert(`computer is win`);
    }
}

const game = new Game();


// class FieldBuild {
//     constructor() {
//         this._createFieldUI();
//     }
//
//     _createFieldUI(){
//         const table = document.createElement('table.table');
//         for(let i = 0 ; i <= 10 ; i ++) {
//             let tr  = table[i].createElement('tr');
//             for (let j = 0 ; j <= 10 ; j++){
//                 let td  = i[j].createElement('td');
//             }
//         }
//         table.addEventListener('click', this.onClick());
//
//         const emptyField = Array.from({length: 10}, (e,i) =>
//             Array.from({length:10}, (e, j) =>
//             [i, j]));
//     }
//
//     // computerCell() {
//     //
//     // }
//     //
//     // userCell() {
//     //     this._activeCell.style.backgroundColor = 'green';
//     //
//     // }
//
// }

// class Randomaiser extends FieldBuild{
//     constructor() {
//         super();
//         this._randomCell();
//     }
// _randomCell() {
//     const shuffledEmptyField = super.emptyField().flat().sort(() =>
//         Math.floor(Math.random() - 0.5));
//     const [rowIndex, cellIndex] = shuffledEmptyField.pop();
//     const td = document.querySelector(`tr:nth-child(${rowIndex}) td:nth-child(${cellIndex})`);
// }
// _activeCell() {
//     const _activeCell = this._randomCell();
//         _activeCell.style.backgroundColor = 'blue';
// }
// }

// _createButtonUI() {
//     const form = document.createElement('form');
//     const buttonEase = document.createElement('button');
//     const buttonMiddle = document.createElement('button');
//     const buttonHard = document.createElement('button');
//     buttonEase.classList.add('btn');
//     buttonMiddle.classList.add('btn');
//     buttonHard.classList.add('btn');
//     buttonEase.classList.add('btn__easy');
//     buttonMiddle.classList.add('btn__middle');
//     buttonHard.classList.add('btn__hard');
//     buttonEase.innerText = 'Ease - 1.5 sec';
//     buttonMiddle.innerText = 'Middle - 1.0 sec';
//     buttonHard.innerText = 'Hard - 0.5 sec';
//     this._form = form;
//     form.append(buttonEase);
//     form.append(buttonMiddle);
//     form.append(buttonHard);
// }

// appendTo(parent) {
//     parent.append(this._table);
//     parent.append(this._form);
// }
