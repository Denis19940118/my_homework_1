
class ToDoTrello {
    constructor(containerColumn) {
        this.cards = [];
        this._containerColumn = document.createElement('div');
        this._containerColumn.classList.add('container__large');
        this._containerColumn.draggable = 'true';
        this._createColumn();
        this._appendTo(document.body);
        this._onClick();
        this._btnAddColumn = document.createElement('button');
        this._btnAddColumn.classList.add('btn');
        this._btnAddColumn.classList.add('btn-add');
        this._btnAddColumn.innerText = 'Add column ...';
        this._containerColumn.append(this._btnAddColumn);
    }

    _add(title) {
        const item = new createCard(title);
        item._appendTo(this._form);
        this.cards.push(item);
    }

    _appendTo(parent) {
        parent.append(this._containerColumn);
        // this._createBtnAddCard();
    }

    _createColumn() {
        const form = document.createElement('form');
        form.classList.add('form-container');
        const helper = document.createElement('p');
        helper.innerText = 'Name form';
        const headingForm = document.createElement('h3');
        headingForm.classList.add('form-heading');
        form.append(helper);
        form.append(headingForm);
        this._form = form;
        this._containerColumn.append(this._form);
        this._createBtnAddCard();
    }

    _createBtnAddCard() {
        const btnAdd = document.createElement('button');
        btnAdd.classList.add('btn');
        btnAdd.classList.add('btn-add');
        btnAdd.innerHTML = 'Add card ...';
        const btnSort = document.createElement('button');
        btnSort.classList.add('btn');
        btnSort.classList.add('btn-sort');
        btnSort.innerHTML = 'Sort card ABC...';
        btnSort.type = 'button';
        btnAdd.type = 'button';
        this._btnAdd = btnAdd;
        this._btnSort = btnSort;
        this._form.append(btnAdd);
        this._form.append(btnSort);
        console.log('hfg');
    }

    _sortCard() {
        this.cards.sort((el1, el2) => {
            const a = el1.getVal();
            const b = el2.getVal();
            console.log(a);
            console.log(b);
            return a > b ? 1 : a < b ? -1 : 0;
        });
        this.cards.forEach(card => card._remove());
        this.cards.forEach(card => card._appendTo(this._form))
    }

    _onClick() {
        this.createCarding = this._add.bind(this);
        this._btnAdd.addEventListener('click' , this.createCarding);
        this._btnAddColumn.addEventListener('click', this._createColumn.bind(this));
        this._containerColumn.addEventListener('dragover', this._drag);
        this._form.addEventListener('dragover', this._drop);
        this._btnSort.addEventListener('click' , this._sortCard.bind(this));
    }

    _drag(event) {
        event.preventDefault();
    }

    _drop (event) {
        event.preventDefault();
    }
}

class createCard {
    constructor(title) {
        const container = document.createElement('div');
        container.classList.add('container');
        container.id = 'card';
        const input = document.createElement('input');
        this._input = input;
        this._input.value = title;
        input.classList.add('container__card-text');
        container.append(input);
        this._container = container;
        this._container.draggable = 'true';
        this._draggable();
        // this._container.ondragstart = this._dragStart(event);
    }

    getVal() {
        return this._input.value;
    }

    _remove() {
        this._container.remove();
    }

    _appendTo(parent) {
        parent.append(this._container);
    }

    _draggable() {
        this._container.addEventListener('drop', this._dragStart);
    }

    _dragStart(event) {
        event.preventDefault();
        const data = event.dataTransfer.getData("text");
        const draggableEl = document.getElementById('card');
        console.log('q');

        const cards = document.querySelectorAll('.container');
        cards.forEach(el => {
            el.dataset.right = el.getBoundingClientRect().right;
            el.dataset.left = el.getBoundingClientRect().left;
            el.dataset.bottom = el.getBoundingClientRect().bottom;
        })

        let rowFactor = 0;
        for (let i = 0; i < cards.length; i++) {
            i !== 0 && i % 3 === 0 ? rowFactor += 3 : "";

            if (cards[2 + rowFactor]) {
                if (event.clientX > +cards[2 + rowFactor].dataset.right) {
                    if (event.clientY <= +cards[i].dataset.bottom) {
                        cards[2 + i].after(draggableEl);
                        rowFactor = 0;
                        break;
                    }
                }
            }

            if (event.clientY <= +cards[i].dataset.bottom && event.clientX <= +cards[i].dataset.left) {
                cards[i].before(draggableEl);
                break;
            } else if (event.clientY <= +cards[i].dataset.bottom && event.clientX <= +cards[i].dataset.right) {
                cards[i].after(draggableEl);
                break;
            }

            if (i === cards.length - 1) {
                cards[cards.length - 1].after(draggableEl);
            }
        }
    }
}
const trello = new ToDoTrello();

















// const cards = [];
//
// class ToDoTrello {
//     cards = [];
//
//     constructor() {
//         this._createColumn();
//         this._appendTo(document.body);
//         this.onClick();
//     }
//
//     _add(title) {
//         const item = this._createCard(title);
//         this.cards.push(item);
//     }
//
//     _appendTo(parent) {
//         parent.append(this._column);
//         // this._questionName();
//         this._createBtnAddCard();
//     }
//
//     // _questionName() {
//     //     const question = prompt('Enter name: ...');
//     //     if (question) {
//     //         document.querySelector('.form-heading').innerHTML = question;
//     //         this._createBtnAddCard();
//     //     } else {
//     //         alert('Wrong name');
//     //         this._questionName();
//     //     }
//     // }
//
//     _createColumn() {
//         const column = document.createElement('div');
//         const form = document.createElement('form');
//         form.classList.add('form-container');
//         const helper = document.createElement('p');
//         helper.innerText = 'Name form';
//         const headingForm = document.createElement('h3');
//         headingForm.classList.add('form-heading');
//         form.append(helper);
//         form.append(headingForm);
//         // form.append(btnAdd);
//         column.append(form);
//         this._form = form;
//         this._column = column;
//     }
//
//     _createCard(title) {
//         const container = document.createElement('div');
//         container.classList.add('container');
//         const input = document.createElement('input');
//         this._input = input;
//         this._input.value = title;
//         input.classList.add('container__card-text');
//         container.append(input);
//         // const render = document.createElement('button');
//         // render.classList.add('btn');
//         // render.innerHTML = 'render...';
//         // render.type = 'button';
//         // label.append(render);
//         // this._label = label;
//         this._form.append(container);
//         // this.render = render;
//         // this.onRender();
//     }
//
//     getVal() {
//             return this._input.value;
//         }
//
//     _createBtnAddCard() {
//         const btnAdd = document.createElement('button');
//         btnAdd.classList.add('btn');
//         btnAdd.classList.add('btn-add');
//         btnAdd.innerHTML = 'Add card ...';
//         const btnSort = document.createElement('button');
//         btnSort.classList.add('btn');
//         btnSort.classList.add('btn-sort');
//         btnSort.innerHTML = 'Sort card ABC...';
//         btnSort.type = 'button';
//         btnAdd.type = 'button';
//         this._btnAdd = btnAdd;
//         this._btnSort = btnSort;
//         this._form.append(btnAdd);
//         this._form.append(btnSort);
//     }
//
//     _sortCard() {
//         console.log('ddf');
//         // const cards = document.querySelectorAll('.container__card-text');
//         // const cardsArr = [...cards];
//         // const arrNew = cardsArr.map(el => el.value);
//         // console.dir(arrNew);
//         // this.remove(cards);
//         this.cards.sort((el1, el2) => {
//             // if (a > b) {
//             //     return 1;
//             // }
//             // if (a < b) {
//             //     return -1;
//             // }
//             // return 0;})
//             const a = el1.getVal();
//             const b = el2.getVal();
//             console.log(a)
//             console.log(b)
//             return a > b ? 1 : a < b ? -1 : 0;
//         });
//         // console.log(arrNew);
//         // this._column.remove();
//         // this._appendTo(document.body);
//
//         // this.cards.forEach(card => card._remove());
//         // this.cards.forEach(card => card._appendTo(this._column))
//     }
//
//     // _remove() {
//     //     this._column.remove();
//     // }
//
//     // _renderoid() {
//     //     const question = prompt('Enter name card: ...');
//     //     if (question) {
//     //         document.querySelector('.label__name-card').innerHTML = question;
//     //     } else {
//     //         alert('Wrong name');
//     //         this._renderoid();
//     //     }
//     // }
//     //
//     // onRender() {
//     //     this.render.addEventListener('click', this._renderoid.bind(this));
//     // }
//
//     onClick() {
//         this.createCarding = this._createCard.bind(this);
//         this._btnAdd.addEventListener('click' , this.createCarding);
//
//         this._btnSort.addEventListener('click' , this._sortCard.bind(this));
//     }
//
// }
//
// const trello = new ToDoTrello();









// const cards = [];
//
// class ToDoList {
//     cards = [];
//
//     constructor() {
//         this._containerDiv = document.createElement('div')
//     }
//
//     _add(title) {
//         const item = this.createCard(title);
//         this._containerDiv.append(this._containerInput);
//         // item.appendTo(this._containerDiv);
//         this.cards.push(item);
//     }
//
//     remove() {
//         this._containerInput.remove();
//     }
//
//     sort() {
//         this.cards.forEach(card => this.remove(card));
//         this.cards.sort(
//             (card1, card2) => {
//             const a = card1.getValue();
//             const b = card2.getValue();
//             return a > b ? 1 : a < b ? -1 : 0;
//         });
//         this.cards.forEach(card => this.appendTo(card));
//         // card.appendTo(this._containerDiv))
//     }
//
//     createCard(title) {
//         this._containerInput = document.createElement('div');
//         this._input = document.createElement('input');
//         this._input.value = title;
//         this._containerInput.append(this._input)
//     }
//
//     getValue() {
//          this._input.value;
//     }
//
//     appendTo(container) {
//             document.body.append(this._containerDiv);
//     }
//
//
// }
// const list = new ToDoList();
// list.appendTo(document.body);
// list._add('s');
// list._add('a');
// list._add('f');
// list._add('z');
// list._add('t');
// list._add('b');
// list._add('u');
























// const cards = [];
//
// class ToDoList {
//     cards = [];
//
//     constructor() {
//         this._el = document.createElement('div')
//     }
//
//     _add(title) {
//         const item = new listItem(title);
//         item.appendTo(this._el);
//         this.cards.push(item);
//     }
//
//     sort() {
//         this.cards.forEach(card => card.remove());
//         this.cards.sort((card1, card2) => {
//             const a = card1.getValue();
//             const b = card2.getValue();
//             return a > b ? 1 : a < b ? -1 : 0;
//         });
//         this.cards.forEach(card => card.appendTo(this._el))
//     }
//
//     appendTo(container) {
//         container.append(this._el);
//     }
// }
//
// class listItem {
//     constructor(title) {
//         this._el = document.createElement('div');
//         this._input = document.createElement('input');
//         this._input.value = title;
//         this._el.append(this._input)
//     }
//
//     remove() {
//         this._el.remove();
//     }
//
//     getValue() {
//         return this._input.value;
//     }
//
//     appendTo(container) {
//         container.append(this._el);
//     }
//
// }
//
// const list = new ToDoList();
// list.appendTo(document.body);
// list._add('s');
// list._add('a');
// list._add('f');
// list._add('z');
// list._add('t');
// list._add('b');
// list._add('u');