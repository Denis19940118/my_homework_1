/**
 * Класс, объекты которого описывают параметры гамбургера.
 *
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 * @param topping     Топинг
 * @throws {HamburgerException}  При неправильном использовании
 */
function Hamburger(size, stuffing) {
    try {
        if (!size) {
            throw new HamburgerException ('Wrong size');
        } else if (!Object.keys(Hamburger.SIZES).includes(size)) {
            throw new HamburgerException (`Wrong size${size}`);
        }
        this._size = size;

        if (!stuffing) {
            throw new HamburgerException ('Wrong stuffing');
        } else if (!Object.keys(Hamburger.STUFFINGS).includes(stuffing)) {
            throw new HamburgerException (`Wrong size${stuffing}`);
        }
        this._stuffing = stuffing;
        this._topping = [];
    } catch (error) {
        return console.error(`${error.name}:${message}`);
    }
}

// Hamburger.prototype._errors = function () {
//     try(this.size !== (Hamburger.SIZE_SMALL || Hamburger.SIZE_LARGE)) {
//         throw new Error('Error in use, enter name (Hamburger.SIZE_SMALL,' +
//         ' Hamburger.SIZE_LARGE)');
//     }
//
//     if (this.stuffing !== Hamburger.STUFFING_SALAD && Hamburger.STUFFING_POTATO && Hamburger.STUFFING_CHEESE) {
//         // || Hamburger.STUFFING_CHEESE
//         throw new Error('Error in use, enter name (Hamburger.STUFFING_CHEESE,' +
//         ' Hamburger.STUFFING_POTATO, Hamburger.STUFFING_SALAD)');
//     }
// };
Hamburger.SIZE_SMALL = 'SIZE_SMALL';
Hamburger.SIZE_LARGE = 'SIZE_LARGE';
Hamburger.SIZES = {
    SIZE_SMALL: {
        name: 'small humburger ',
        price: 50,
        kcal: 20
    },
    SIZE_LARGE: {
        name: 'large humburger ',
        price: 100,
        kcal: 40
    },
};
Hamburger.STUFFING_CHEESE = 'STUFFING_CHEESE';
Hamburger.STUFFING_SALAD = 'STUFFING_SALAD';
Hamburger.STUFFING_POTATO = 'STUFFING_POTATO';
Hamburger.STUFFINGS = {
    STUFFING_CHEESE: {
        name: ' with stuffing cheese ',
        price: 10,
        kcal: 20
    },
    STUFFING_SALAD: {
        name: ' with stuffing salad ',
        price: 20,
        kcal: 5
    },
    STUFFING_POTATO: {
        name: ' with stuffing potato ',
        price: 15,
        kcal: 10
    },
};
Hamburger.TOPPING_MAYO = 'TOPPING_MAYO';
Hamburger.TOPPING_SPICE = 'TOPPING_SPICE';
Hamburger.TOPPINGS = {
    TOPPING_MAYO: {
        name: ' and with topping mayo',
        price: 20,
        kcal: 5
    },
    TOPPING_SPICE: {
        name: ' and with topping spice',
        price: 15,
        kcal: 0
    },
};
/**
 * Добавить добавку к гамбургеру. Можно добавить несколько
 * добавок, при условии, что они разные.
 *
 * @param topping     Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.addTopping = function (topping) {
    try {
        if (!topping) {
            throw new HamburgerException ('Wrong getting topping');
        } else if (!Object.keys(Hamburger.TOPPINGS).includes(topping)) {
            throw new HamburgerException (`Wrong size${topping}`);
        } else if (this._topping.includes(topping)) {

            throw new HamburgerException('this topping is duplicate');
        }
        console.log(topping);
        return this._topping.push(topping);
    } catch (error) {
        console.error(`${error.name}:${error.message}`);
    }
};

/**
 * Убрать добавку, при условии, что она ранее была
 * добавлена.
 *
 * @param topping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.removeTopping = function (topping) {
    try {
        let arr = this._topping;
        if (!arr.includes(topping)) {
            throw new HamburgerException('impassible this operation ')
        }
        arr = arr.filter(item => item !== topping);
        if (arr.includes(topping)) {
            console.log(arr);
            console.log(this._topping);
            throw new HamburgerException("Topping don\`t remove")
        }
        return arr;
    } catch (error) {
        console.error(`${error.name}:${error.message}`)
    }
};
/**
 * Получить список добавок.
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */
Hamburger.prototype.getToppings = function () {
    return this._topping;
};
/**
 * Узнать размер гамбургера
 */
Hamburger.prototype.getSize = function () {
    return this._size;
};
/**
 * Узнать начинку гамбургера
 */
Hamburger.prototype.getStuffing = function () {
    let arrayStuffing = [];
    arrayStuffing.push(this._stuffing);
    arrayStuffing.filter(el => {
        // console.log(el);
        return el !== undefined;
    });
    return arrayStuffing
};
/**
 * Узнать цену гамбургера
 * @return {Number} Цена в тугриках
 */
Hamburger.prototype.calculatePrice = function () {
    let res;
    let size = Hamburger.SIZES[this._size].price;
    let stuffing = Hamburger.STUFFINGS[this._stuffing].price;
    // let topping = this._topping.filter(el => {
    //     return el !== undefined
    // });
    let toppi = this._topping.map((el) => Hamburger.TOPPINGS[el].price);
    // console.log(size);
    // console.log(stuffing);
    // console.log(topping);
    // console.log(toppi);

    toppi.push(size);
    toppi.push(stuffing);
    // console.log( toppi );

    return res = toppi.reduce((sum, current) => sum + current, 0);

};
// console.log( burger.calculatePrice());

/**
 * Узнать калорийность
 * @return {Number} Калорийность в калориях
 */
Hamburger.prototype.calculateCalories = function () {
    let res;
    // let size = Hamburger.SIZES[this.size].kcal;
    // let stuffing = Hamburger.STUFFINGS[this.stuffing].kcal;
    // let topping = Hamburger.TOPPINGS[this.topping].filter(el => {
    //     return el !== undefined
    // });
    let toppi = this._topping.map((el) => Hamburger.TOPPINGS[el].kcal);
    // console.log(size);
    // console.log(stuffing);
    // console.log(topping);
    // console.log(toppi);

    toppi.push(Hamburger.SIZES[this._size].kcal, Hamburger.STUFFINGS[this._stuffing].kcal);
    // console.log( toppi );

    return res = toppi.reduce((sum, current) => sum + current, 0);
};

// console.log(burger.calculateCalories());
function HamburgerException(message) {
    this.message = message;
}


// маленький гамбургер с начинкой из сыра
var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_SALAD);
// добавка из майонеза
hamburger.addTopping(Hamburger.TOPPING_MAYO);
// спросим сколько там калорий
console.log("Calories: %f", hamburger.calculateCalories());
// сколько стоит
console.log("Price: %f", hamburger.calculatePrice());
// я тут передумал и решил добавить еще приправу
// hamburger.addTopping(Hamburger.TOPPING_SPICE);
// А сколько теперь стоит?
console.log("Price with sauce: %f", hamburger.calculatePrice());
// Проверить, большой ли гамбургер?
console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
// Убрать добавку
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log("Have %d toppings", hamburger.getToppings().length);
// 1
// /**
//  * Представляет информацию об ошибке в ходе работы с гамбургером.
//  * Подробности хранятся в свойстве message.
//  * @constructor
//  */
//
// ```javascript
//    // маленький гамбургер с начинкой из сыра
//    var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
//    // добавка из майонеза
//    hamburger.addTopping(Hamburger.TOPPING_MAYO);
//    // спросим сколько там калорий
//    console.log("Calories: %f", hamburger.calculateCalories());
//    // сколько стоит
//    console.log("Price: %f", hamburger.calculatePrice());
//    // я тут передумал и решил добавить еще приправу
//    hamburger.addTopping(Hamburger.TOPPING_SPICE);
//    // А сколько теперь стоит?
//    console.log("Price with sauce: %f", hamburger.calculatePrice());
//    // Проверить, большой ли гамбургер?
//    console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
//    // Убрать добавку
//    hamburger.removeTopping(Hamburger.TOPPING_SPICE);
//    console.log("Have %d toppings", hamburger.getToppings().length); // 1
//    ```
//
// //- При неправильном использовании класс сообщает об этом с помощью выброса исключения.
//
//     ```javascript
//    // не передали обязательные параметры
//    var h2 = new Hamburger(); // => HamburgerException: no size given
//
//    // передаем некорректные значения, добавку вместо размера
//    var h3 = new Hamburger(Hamburger.TOPPING_SPICE, Hamburger.TOPPING_SPICE);
//    // => HamburgerException: invalid size 'TOPPING_SAUCE'
//
//    // добавляем много добавок
//    var h4 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
//    hamburger.addTopping(Hamburger.TOPPING_MAYO);
//    hamburger.addTopping(Hamburger.TOPPING_MAYO);
//    // HamburgerException: duplicate topping 'TOPPING_MAYO'
//
//    ```
// Hamburger.SIZE_SMALL = function () {
//     if (this.size === 'small') {
//         return 50;
//     }
// }
// Hamburger.SIZE_LARGE = function () {
//     if (this.size === 'large') {
//         return 50;
//     }
// }