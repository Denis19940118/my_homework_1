const {src, dest, parallel} = require('gulp');
const sass = require('gulp-sass');
sass.compiler = require('node-sass');

exports.default = () => src("style.scss")
.pipe(sass())
.pipe(dest("./"));
