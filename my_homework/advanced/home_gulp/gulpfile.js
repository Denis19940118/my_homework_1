
let project_folder = "dist";
let source_folder = "src";


let path = {
    build: {
        html: project_folder + "/",
        css: project_folder + "/css/",
        js: project_folder + "/js/",
        img: project_folder + "/assets/",
    },
    src: {
        html: [source_folder + "/*.html", "!" + source_folder + "/_*.html"],
        css: source_folder + "/scss/style.scss",
        js: source_folder + "/js/main.js",
        img: source_folder + "/assets/**/*.{jpg,png,svg,gif,ico,webp}",

    },
    watch: {
        html: source_folder + "/**/*.html",
        css: source_folder + "/scss/**/*.scss",
        js: source_folder + "/js/**/*.js",
        img: source_folder + "/assets/**/*.{jpg,png,svg,gif,ico,webp}",
    },
    clean: "./" + project_folder + "/",
};


let {src, dest, series, parallel} = require('gulp'),
    gulp = require('gulp'),
    browsersync = require("browser-sync").create(),
    fileinclude = require('gulp-file-include'),
    del = require("del"),
    scss = require("gulp-sass"),
    autoprefixer = require("gulp-autoprefixer"),
    group_media = require("gulp-group-css-media-queries"),
    clean_css = require("gulp-clean-css"),
    rename = require("gulp-rename"),
    uglify = require("gulp-uglify-es").default,
    imagemin = require("gulp-imagemin"),
    webp = require("gulp-webp"),
    webphtml = require("gulp-webp-html"),
    webpcss = require("gulp-webpcss");



function browserSync(param) {
    browsersync.init({
        server: {
            baseDir: "./" + project_folder + "/"
        },
        port: 3000,
        notify: false
    })
}

function html() {
    return src(path.src.html)
        .pipe(fileinclude())
        .pipe(webphtml())
        .pipe(dest(path.build.html))
        .pipe(browsersync.stream())
}
function js () {
    return src(path.src.js)
        .pipe(fileinclude())
        .pipe(
            uglify()
            )
        .pipe(
            rename({
                extname: ".min.js"
            })
            )
        .pipe(dest(path.build.js))
        .pipe(browsersync.stream())
}


function images () {
    return src(path.src.img)
        .pipe(
            webp({
                quality: 70
            })
            )
        .pipe(dest(path.build.img))
        .pipe(src(path.src.img))
        .pipe(
            imagemin({
                progressive: true,
                svgoPlugins:[{removeViewBox: false}],
                interlaced: true,
                optimizationLevel: 3
            })
            )
        .pipe(dest(path.build.img))
        .pipe(browsersync.stream())
}

function watchFiles(params) {
    gulp.watch([path.watch.html], html);
    gulp.watch([path.watch.css], css);
    gulp.watch([path.watch.js], js);
    gulp.watch([path.watch.img], images);
}

function clean(params) {
    return del(path.clean);
}

function css() {
    return src(path.src.css)
        .pipe(
            scss({
                outputStyle: "expanded"
            })
            )
        .pipe(
            group_media()
            )
        .pipe(
            autoprefixer ({
                overrideBrowserslist: ["last 5 versions"],
                cascade: true
            })
            )
        .pipe(webpcss())
        .pipe(dest(path.build.css))
        .pipe(clean_css())
        .pipe(
            rename({
                extname: ".min.css"
            })
            )
        .pipe(dest(path.build.css))
        .pipe(browsersync.stream())
}

let build = gulp.series(clean,  gulp.parallel(js, css, html, images));
let watch = gulp.parallel(build, watchFiles, browserSync);


exports.dev = watch;
exports.images = images;
exports.js = js;
exports.css = css;
exports.html = html;
exports.build = build;
exports.watch = watch;



// const gulp = require('gulp'),
//     sass = require('gulp-sass'),
//     autoprefixer = require('gulp-autoprefixer'),
//     cleanCSS = require('gulp-clean-css'),
//     cleaner = require('gulp-clean'),
//     concat = require('gulp-concat'),
//     minify = require('gulp-js-minify'),
//     uglify = require('gulp-uglify'),
//     pipeline = require('readable-stream').pipeline,
//     imagemin = require('gulp-imagemin'),
//     browserSync = require('browser-sync').create();
// const path = {
//     dist:{
//         html:'dist',
//         css:'dist/css',
//         js:'./dist/js',
//         img : 'dist/assets',
//         // ico: 'dist/favicon',
//         self:'dist'
//     },
//     src : {
//         html:'src/*.html',
//         scss : 'src/scss/**/*.*',
//         js : './src/js/**.js',
//         img: 'src/assets/**/**/*.*',
//         // ico: 'src/favicon/*.*',
//     }
// };
// /**************** F U N C T I O N S ***************/
// const htmlBuild = () => (
//     gulp.src(path.src.html)
//         .pipe(gulp.dest(path.dist.html))
//         .pipe(browserSync.stream())
// );
// const scssBuild = () => (
//     gulp.src(path.src.scss)
//         .pipe(sass().on('error', sass.logError))
//         .pipe(autoprefixer(['> 0.01%', 'last 100 versions']))
//         .pipe(gulp.dest(path.dist.css))
//         .pipe(browserSync.stream())
// );
// const imgBuild = () => (
//     gulp.src(path.src.img)
//         .pipe(imagemin())
//         .pipe(gulp.dest(path.dist.img))
//         .pipe(browserSync.stream())
// );
//
// // const jsBuild = () => (
// //     gulp.src(path.src.js)
// //         .pipe(concat('script.js'))
// //         .pipe(minify())
// //         .pipe(uglify())
// //         .pipe(gulp.dest(path.dist.js))
// //         .pipe(browserSync.stream())
// // );
//
// function copyJs() {
//     return gulp.src(path.src.js)
//         .pipe(concat('script.js'))
//         .pipe(minify())
//         .pipe(uglify())
//         .pipe(gulp.dest(path.dist.js))
//         .pipe(browserSync.reload({ stream: true })); // added this line
// }
//
// const cleanProd = () =>(
//     gulp.src(path.dist.self, {allowEmpty: true})
//         .pipe(cleaner())
//         .pipe(cleanCSS({compatibility: 'ie8'}))
//         .pipe(browserSync.stream())
// );
// // const icoBuild = () => (
// //     gulp.src(path.src.ico)
// //         .pipe(gulp.dest(path.dist.ico))
// //         .pipe(browserSync.stream())
// // );
// /****************** W A T C H E R ***************/
// const watcher = () => {
//     browserSync.init({
//         server: {
//             baseDir: "./dist"
//         }
//     });
//     gulp.watch(path.src.html, htmlBuild).on('change',browserSync.reload);
//     gulp.watch(path.src.scss, scssBuild).on('change',browserSync.reload);
//     gulp.watch(path.src.js, copyJs).on('change',browserSync.reload);
//     gulp.watch(path.src.img, imgBuild).on('change',browserSync.reload);
// };
// /**************** T A S K S ****************/
// gulp.task('default',gulp.series(
//     cleanProd,
//     htmlBuild,
//     scssBuild,
//     imgBuild,
//     copyJs,
//     // icoBuild,
//     watcher,
//     ));