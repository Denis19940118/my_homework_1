// let phrase = "Hello";
//
// if (true) {
//     let user = "John";
//
//     alert(`${phrase}, ${user}`)
// }

// Пути создания IIFE
//Во всех перечисленных случаях мы объявляем Function Expression и немедленно выполняем его.
// (function() {
//     alert("Скобки вокруг функции");
// })();
//
// (function() {
//     alert("Скобки вокруг всего");
// }());
//
// !function() {
//     alert("Выражение начинается с логического оператора NOT");
// }();
//
// +function() {
//     alert("Выражение начинается с унарного плюса");
// }();

// function f() {
//     let value = 123;
//
//     function g() { alert(value); }
//
//     return g;
// }
//
// let g = f(); // g доступно и продолжает держать внешнее лексическое окружение в памяти
// Обратите внимание, если f() вызывается несколько раз и возвращаемые функции сохраняются, тогда все соответствующие объекты лексического окружения продолжат держаться в памяти. Вот три такие функции в коде ниже:
//
//     function f() {
//         let value = Math.random();
//
//         return function() { alert(value); };
//     }
//
// // три функции в массиве, каждая из них ссылается на лексическое окружение
// // из соответствующего вызова f()
// let arr = [f(), f(), f()];
// Объект лексического окружения умирает, когда становится недоступным (как и любой другой объект). Другими словами, он существует только до того момента, пока есть хотя бы одна вложенная функция, которая ссылается на него.
//
//     В следующем коде, после того как g станет недоступным, лексическое окружение функции (и, соответственно, value) будет удалено из памяти;
//
// function f() {
//     let value = 123;
//
//     function g() { alert(value); }
//
//     return g;
// }
//
// let g = f(); // пока g существует,
// // соответствующее лексическое окружение существует
//
// g = null; // ...а теперь память очищается

// function f() {
//     let value = Math.random();
//
//     function g() {
//         debugger; // в консоли: напишите alert(value); Такой переменной нет!
//     }
//
//     return g;
// }
//
// let g = f();
// g();
//
// let value = "Сюрприз!";
//
// function f() {
//     let value = "ближайшее значение";
//
//     function g() {
//         debugger; // в консоли: напишите alert(value); Сюрприз!
//     }
//
//     return g;
// }
//
// let g = f();
// g();
// Эту особенность V8 полезно знать. Если вы занимаетесь отладкой в Chrome/Opera, рано или поздно вы с ней встретитесь.
    //
    // Это не баг в отладчике, а скорее особенность V8. Возможно со временем это изменится. Вы всегда можете проверить это, запустив пример на этой странице.


// let person = {
//     name: 'john',
//     greet: function () {
// return " Hi! My name is " + person.name
//     }
// }
//
// console.log(person.greet());

// let greet = function () {
// //     return " Hi! My name is " + this.name
// // };
// //
// // let person = {
// //     name: 'john',
// //    greet: greet
// // };
// // let anotherPerson = {
// //     name: 'Bob',
// //     greet: greet
// // };
// //
// // console.log(person.greet());
// // console.log(anotherPerson.greet.call(person,"Bonjour"));
// // console.log(anotherPerson.greet.apply(person,["Bonjour"]));
// // let bound = greet.bind(anotherPerson);
// // console.log(bound("Hello there"));

// // ======================  ================================ ===============
// //===============constructor=========================================
// // ====================================================================
// let Man = function (name) {
//     this.name;
//     this.canSpeak = true;
//
//     // let priv = '34';
//
// //     this.sayHello = function () {
// // return 'Hi! my name, ' + this.name + '. Me ' + priv + 'years.';
// //     };
//
//     // return {name: "Ann Borisovna" };
// };
//
//
// Man.prototype.sayHello = function ( ) {
//     return 'Hi! my name, ' + this.name + '. Me ' + 34 + 'years.';
//
// }
//
// let vanya = new Man('Vanya');
// let petr = new Man('Petr');
// // console.log(vanya);
// // console.log(vanya.name);
// // console.log(vanya.canSpeak);
// // console.log(vanya.sayHello());
// // console.log(petr.sayHello());
// // console.log(petr.priv);
//
// let MyApp = {
//      Man : function (name) {
//         this.name;
//         this.canSpeak = true;
//     }
//     };
//
// let gena = new MyApp.Man("gena");
// console.log(gena);
//
// console.log(gena instanceof MyApp.Man);
// console.log(vanya instanceof Man);
//
// console.log(gena.constructor);
// console.log(vanya.constructor);

//родительская функция
function f1(x) {
    //внутренняя функция f2
    function f2(y) {
        return x + y;
    }
    //родительская функция возвращает в качестве результата внутреннюю функцию
    return f2;
}
var c1 = f1(2);
var c2 = f1(5);
//отобразим детальную информацию о функции c1
console.dir(c1);
//отобразим детальную информацию о функции c2
console.dir(c2);
console.log(c1(5)); //7
console.log(c2(5)); //10