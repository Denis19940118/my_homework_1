
//замыкание
// let firstFunc = function () {
//     let index = 5;
//     return function () {
//         return index;
//     }
// };
//
// let secondFunc = function () {
//
//     let index = 15;
//
//     console.log(firstFunc()());
//     let oldIndex = firstFunc()();
//
//     if(index > oldIndex) {
//         alert("bigger!");
//     }
// };
//
// secondFunc();
//
// console.log(index);//error

//es5
// let obj = {
//     width : 100,
// };


// //es6
// let { width, height }= {
//     width : 100,
//     height: 200,
// };
// console.log(width);
// console.log(height);
//
// function generateObj() {
// return {
//     width : 100,
//     height: 200,
// }
// }
//
// let {width: containerWidth, height: containerHeight} = generateObj();
//
// console.log(containerWidth);
// console.log(containerHeight);
//
// let obj = {
//     id: '123',
//     attributes: {
//         width: 100,
//         height: 200,
//     }
// }
//                        по умолчанию если значения не заданы
// let { id, attributes: {width = 50, height = 60}} = obj;
//
//
// console.log(width);
// console.log(height);
//
// let [red = 'my red color', yellow = 'my yellow '] = ['red', 'yellow'];
// console.log(red);
// console.log(yellow);
//
//
// let [first, , , , fifth] = ['red', 'yellow', 'green', 'blue', 'orange'];
// console.log(first);
// console.log(fifth);
//
// function getSizes({ width = 50, height = 60 }) {
//     console.log(width, height);
// }
//
// getSizes({
//     width: 100,
//     height: 50,
// });




// function hello() {
// alert(hello);
// // return 9;
//     //undefined
// }
// // hello();
// // console.log(5 + hello());
//
// function summa(a, b) {
//     let c = 50;
//     a = a || 10; //undefined или 10
//     b = b || 20;
//     alert(a + b);
//      // return (a + b);
//
// }
//
// console.log( summa(4, 5));
// alert(c);
//
// // document.getElementById('summa')
// //     // .onclick = summa(15,15);
// //     .onclick = summa;
//
// document.getElementById('summa').onclick = function () {
// summa(12,6);
// }
//
// let d = function () {
// alert('work');
// }
// d();



// let obj = {
//     name: 'Table',
// };
//
// let newObj = {
//     name: 'Bag',
//     test: function () {
// console.log('test');
//     }
// };
//
// let Foo = function () {
//     this.name = 'Fuu';
//     // this.__proto__= Foo.prototype;
//
// };
// let boo = new Foo;
//
// Foo.prototype = newObj;
//
// let boo2 = new Foo;
// console.log( boo2 );
//
//
//
//
//
// // console.log(boo);
// // console.log( boo.__proto__ ===Foo.prototype);

//
//
// let animal = {
//     canRun: true,
// };
//
// let Wolf = function () {
//     this.name = 'Wolf'
// };
//
// let Grey = function () {
//     this.color = 'Grey';
// };
//
// let Black = function () {
//     this.color ='Black';
// };
//
// let Chicken = function () {
//     this.name = 'Chicken';
//     this.haveWings = true;
// };
//
// Wolf.prototype = animal;
// Chicken.prototype = animal;
// Chicken.prototype.Kukareku = function () {
//     console.log( 'Kukareku!' );
// };
//
// let wolfy = new Wolf();
// let chicky = new Chicken();
// chicky.test = function () {
//     console.log('test')
// };
//
// delete chicky.name;
// chicky.canRun = false;
// delete chicky.canRun;
//
// console.log(chicky);
//
// let chickySib = new Chicken();
//
// console.log(chicky);
//
// Chicken.prototype = {
//     sibling: 'induk',
// };
//
// Grey.prototype = wolfy;
// Black.prototype = wolfy;
//
// let wolfyGrey = new Grey();
// let wolfyBlack = new Black();
//
// // console.log( wolfy );
// // console.log( wolfyGrey );
// // console.log( wolfyBlack );
// // console.log( chicky );

// class User {
//
//     constructor(name) {
//         // вызывает сеттер
//         this.name = name;
//     }
//
//     get name() {
//         return this._name;
//     }
//
//     set name(value) {
//         if (value.length < 4) {
//             alert("Имя слишком короткое.");
//             return;
//         }
//         this._name = value;
//     }
//
// }
//
// let user = new User("Иван");
// alert(user.name); // Иван
//
// user = new User(""); // Имя слишком короткое.







