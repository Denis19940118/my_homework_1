//
// function makeCounter() {
// let count = 0;
//
//     function counter() {
//
//         return count++;
//     };
//
//     counter.set = value => count = value;
//
//     counter.decrease = () => count--;
//
//     return counter;
// }
//
// let counter = makeCounter();
//
// alert( counter() ); // 0
// alert( counter() ); // 1
//
// counter.set(10); // установить новое значение счётчика
//
// alert( counter() ); // 10
//
// counter.decrease(); // уменьшить значение счётчика на 1
//
// alert( counter() ); // 10 (вместо 11)

// function sum ( a ) {
// let currentVar = a;
//
// function varB( b) {
//     currentVar += b;
//     return varB
// }
//
// varB.toString = function () {
// return currentVar;
// };
//
// return varB;
//
// }
//
// alert( sum(1)(2) ); // 3
// alert( sum(5)(-1)(2) ); // 6
// alert( sum(6)(-1)(-2)(-3) ); // 0
// alert( sum(0)(1)(2)(3)(4)(5) ); // 15


// function f() {
//
//
//     function Hi(user) {
//         alert(`My name ${this.name} and age old me ${this.age} my phone ${this.phone}`);
//     }
//
//     function User(name, age, phone) {
//
//         this.name = name;
//         this.age = age;
//         this.phone = phone;
//     }
//
// // let user = {
// //     name: 'Tanya',
// //     age: 45,
// // };
//
//     let female = new User("Tanya", 37, +380675493049);
//
//     Hi.call(female);
//
// }
//
// f();


// var o = { a: 0 };
//
// Object.defineProperties(o, {
//     'b': { get: function() { return this.a + 1; } },
//     'c': { set: function(x) { this.a = x / 2; } }
// });
//
// o.c = 10; // Запускает сеттер, который присваивает 10 / 2 (5) свойству 'a'
// console.log(o.b); // Запускает геттер, который возвращает a + 1 (тоесть var o = { a: 0 };
//
// Object.defineProperties(o, {
//     'b': { get: function() { return this.a + 1; } },
//     'c': { set: function(x) { this.a = x / 2; } }
// });
//
// o.c = 10; // Запускает сеттер, который присваивает 10 / 2 (5) свойству 'a'
// console.log(o.b); // Запускает геттер, который возвращает a + 1 (тоесть
//
// var o = {
//     a: 7,
//     get b() { return this.a + 1; },
//     set c(x) { this.a = x / 2; }
// };