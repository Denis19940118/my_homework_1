let user = {
    name : 'Frank',
    id: 42345,
    lastVisit: Date.now(),
    friends: function () {
return {
    name: this.name,
    lastVisit: this.lastVisit
}
    }
};

let userData = JSON.stringify(user);
console.log(userData);
const table = document.getElementById('table');
table.innerHTML = JSON.parse(userData).map(item => `<li>${item}</li>`);
console.log(JSON.parse(userData));