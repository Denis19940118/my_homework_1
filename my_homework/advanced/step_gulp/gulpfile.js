const gulp = require ('gulp'),
 sass = require ('gulp-sass'),
 scss = require ('gulp-scss'),
 autoprefixer = require ( 'gulp-autoprefixer'),
 clean = require ('gulp-clean'),
 cleanCss = require('gulp-clean-css'),
 uncss = require ('uncss'),
 postcss = require ('gulp-postcss'),
 postcssUncss = require ('postcss-uncss'),
 browserSync = require ('browser-sync').create(),
 concat = require ('gulp-concat'),
 imagemin = require ('gulp-imagemin'),
 minJs = require ('gulp-js-minify'),
 del = require ('del'),
 readable = require ('readable-stream').pipeline,
 uglify = require ('gulp-uglify');

const path = {
    dist: {
        html:'dist',
        css:'dist/css',
        js:'./dist/js',
        img : 'dist/assets',
        ico: 'dist/assets/favicon',
        self:'dist'
    },
    src: {
        html:'src/*.html',
        scss : 'src/scss/**/*.*',
        js : './src/js/**.js',
        img: 'src/assets/**/**/*.*',
        ico: 'src/assets/favicon/*.*',

    },
};
                                 /*=============function================*/
function htmlBuild() {
    return gulp.src(path.src.html)
        .pipe(gulp.dest(path.dist.html))
        .pipe(browserSync.stream())
}
function scssBuild() {
return gulp.src(path.src.scss)
    .pipe(sass().on('error', sass.logError))
    // .pipe(postcss([postcssUncss({html: [path.dist.html]})]))
    .pipe(autoprefixer(['> 0.01%', 'last 100 versions']))
    .pipe(gulp.dest(path.dist.css))
    .pipe(browserSync.stream())
}
function jsBuild() {
 return gulp.src(path.src.js)
     .pipe(concat('script.js'))
     .pipe(minJs())
     .pipe(uglify())
     .pipe(gulp.dest(path.dist.js))
     .pipe(browserSync.stream())
}
// function assetsBuild() {
// return gulp.src(path.src.img)
//     .pipe(imagemin())
//     .pipe(gulp.dest(path.dist.img))
//     .pipe(browserSync.stream())
// }
const imgBuild = () => (
    gulp.src(path.src.img)
        .pipe(imagemin())
        .pipe(gulp.dest(path.dist.img))
        .pipe(browserSync.stream())
);
function iconBuild() {
return gulp.src(path.src.ico)
    .pipe(imagemin())
    .pipe(gulp.dest(path.dist.ico))
    .pipe(browserSync.stream())
}

const cleanProd = () => (
 gulp.src(path.dist.self, {allowEmpty: true})
    .pipe(clean())
    .pipe(cleanCss({compatibility: 'ie8'}))
    .pipe(browserSync.stream())
);

const serve = () => {
 browserSync.init({
    server: {
        baseDir: "./dist"
    }
});
    gulp.watch(path.src.html, htmlBuild).on('change', browserSync.reload);
    gulp.watch(path.src.scss, scssBuild).on('change', browserSync.reload);
    gulp.watch(path.src.js, jsBuild).on('change', browserSync.reload);
    gulp.watch(path.src.img, imgBuild).on('change',browserSync.reload);
    gulp.watch(path.src.ico, iconBuild).on('change',browserSync.reload);
};

gulp.task('default',gulp.series(
    cleanProd,
    htmlBuild,
    scssBuild,
    imgBuild,
    jsBuild,
    iconBuild,
    serve
    ));