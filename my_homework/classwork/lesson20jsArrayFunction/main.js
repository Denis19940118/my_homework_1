 // синтаксис стрелосчной функции

 // (a,b,c)=>{
 //  //
 //  // }


 // const x = (a,b,c) => {
    // a++;
    // a*=3;
    // return (a + 1)* 3;
    //  return (a++)*3;
 // };


 // const x = a => (a + 1) * 3;

 // function x(a,b,c) {... }
 // const x = function (a,b,c) {
//....
//  }

//
//  const obj = {};
// obj.name = "Name";
// // obj.getName = function () {
// // return this.name;
// // };
//
//  obj.getName =  () => this.name;
//
// console.log(obj.getName());

//  const user = {
//     name: "My name",
//      getNameWithDelay(){
//         // console.log(1);
//         //  setTimeout(console.log(1),  2000);
//     setTimeout(() => {
//         console.log(this.name);
//     }, 2000);
//     }
//  };
//
//
//                                      //ЗАМЫКАНИЯ
// //const  sum  = a=> (b => a + b) ;          (b => a + b)//эта часть замыкание , потому что в себе использует переменную с наруже, тоесть а!!!
// const  sum  = a=> b => a + b ;
// console.log(sum(5)(3));
//
// const addTo5 = sum(5);
// const minus10 = sum(-10);
//
// setTimeout(() => {
//     console.log(addTo5(100));
//     // console.log(minus10(10));
//     // console.log(minus10(15));
// },1000);
// user.getNameWithDelay();

//  mySetTimeout(function () {
// user.getNameWithDelay();
//  });
//
//  mySetTimeout(() => user.getNameWithDelay())
// // function mySetTimeout(fn) {
// fn()
// }


//  function sum( a, b, c = 0) {
// return a + b + c;
//  }

 // function sum(a, b, c){
 //     c = c || 0;
 // }

 //  function sum( a, b, c = 1) {
 // return a * b * c;
 //  }
 //                   parametry
 // function multiply(a, b = 1, c){
 // //    if (c === undefined){   //повека на undefined
 // //
 // // }
 //
 //    // if (typeof c === "undefined"){          //повека на undefined
 //    // }
 //
 //    // c = c || 1;
 //     console.log(arguments.length);
 //
 //    return a * b * c;
 // }
 // console.log(multiply(1, 2, 0));
 //             arguments
 // multiply(1, 2, 3)
 // multiply(1, 2)
 // multiply(1,)



 // const user = {
 //    name: "My name",
 //     data : {
 //        agr : 10
 //     }
 // };

// let name = user.name;
//  let {name} = user

 // let {data: {age}} = user;
 // let age = user.data.age;
 // age++;
 // console.log(user);

 // let {data} = user;
 // let data = user.data;
 // data.age++;
 // console.log(user);

  // let {name: n, data: {age: a}, prop: p = 123} = user ;
 // let {property: p = 0, data: {age}} = user;
 // console.log(n, a, p);


//  function generateCoord(x, y){
//     // return x * y;
// return{
//     lezing : x,
//     age : y,
//
// }
// }

// const generateCoord = (x, y) => x * y ;

 // const generateCoord = ( x, y ) => ({x,y, });        //({x:x, y:y})



// const generateCoord = function (x, y, z) {
// if (z === undefined){
//     return console.log(generateCoord(3, 2))
// }
// else {
//     return console.log(generateCoord(3, 2, 1))
// }
// };
 //
 // const generateCoord = ( x, y, z ) => (z === undefined ? {x,y,} : {x,y,z});
 // console.log(generateCoord(3, 2))
 // console.log(generateCoord(3, 2, 1))

//  const generateCoord = ( x, y, z ) => (z === undefined ? {x,y} : {x,y,z});
//
// // const sum = (a, b) => ({x: a.x + b.x, y: a.y + b.y});
//
//  const sum =({x:ax, y:ay},{x:bx,  y:by}) => ({x: ax + bx, y: ay + by});
//
//  //  sum = a => b=> a + b;
//
//  const a = generateCoord(3, 2);
//  const b = generateCoord(4, 1);
 // console.log(a);       //3,2
 // console.log(b);       //4,1
 // console.log(sum(a,b)); //{7,3}



 // const generateCoord = ( x, y, z ) => (z === undefined ? {x,y} : {x,y,z});
 //  // const sum =({x:ax=0, y:ay=0},{x:bx=0,  y:by=0}) => ({x: ax + bx, y: ay + by});
 //  //
 //  // const a = generateCoord(3, 2);
 //  // const b = generateCoord(4, 1);
 //  //
 //  // console.log(sum({x:3}, {y:4}));
 //  // console.log(sum({y:3}, {x:4}));
 //  // console.log(sum({x:3}, {x:4}));




 const generateCoord = ( x, y, z ) => {
     if (z === undefined || (typeof z === 'object' && !z ['3d']))
         return {x, y};
     if (typeof z === "object" && z['3d']) {
         z = 0;
         return {x, y, z};
     }
 };
 console.log(generateCoord(3, 2));
 console.log(generateCoord(3, 2, 5));
 console.log(generateCoord(3, 2, {'3d': true}));
 console.log(generateCoord(3, 2, {'3d': false}));
