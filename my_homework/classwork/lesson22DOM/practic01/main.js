// document.body.myData = {
//     name: 'Caesar',
//     title: 'Imperator'
// };
//
// alert(document.body.myData.title); // Imperator



// document.body.sayTagName = function() {
//     alert(this.tagName);
// };
// document.body.sayTagName();



// Element.prototype.sayHi = function () {
// alert(`Hello, I'm ${this.tagName}`);
// };
//
// document.documentElement.sayHi(); //Hello, I'm HTML
// document.body.sayHi(); //Hello, I'm BODY

// alert(document.body.getAttribute('something')); // non-standard

// alert( elem.getAttribute('About'));
//
// elem.setAttribute('Test', 123);
//
// alert( elem.outerHTML);
//
// for ( let attr of elem.attributes) {
//     alert(`${attr.name} = ${attr.value}`);
// }



// let input = document.querySelector('input');
//// атрибут => свойство
// input.setAttribute('id', 'id');
// alert(input.id);
//// свойство => атрибут
// input.id = 'newId';
// alert(input.getAttribute('id')); // newId (обновлено)

//
// let input = document.querySelector('input');
// // атрибут => значение
// input.setAttribute('value', 'text');
// alert(input.value);
// // свойство => атрибут
// input.value = 'newValue';
// alert(input.getAttribute('value'));// text (не обновилось!)



// alert(input.getAttribute('checked')); // значение атрибута: пустая строка
// alert(input.checked); // значение свойства: true
// строка
// alert(div.getAttribute('style')); // color:red;font-size:120%
//
// // объект
// alert(div.style); // [object CSSStyleDeclaration]
// alert(div.style.color); // red


// // атрибут
// alert(a.getAttribute('href')); // #hello
//
// // свойство
// alert(a.href ); // полный URL в виде http://site.com/page#hello

// Нестандартные атрибуты, dataset
// // код находит элемент с пометкой и показывает запрошенную информацию
// let user = {
//     name: "Pete",
//     age: 25
// };
//
// for(let div of document.querySelectorAll('[show-info]')) {
//     // вставить соответствующую информацию в поле
//     let field = div.getAttribute('show-info');
//     div.innerHTML = user[field]; // сначала Pete в name, потом 25 в age
// }
// let user = {
//     name: 'Pate',
//     age: 25,
// }
// for ( let div of document.querySelectorAll(['show-info'])){
//     let field = div.getAttribute('show-info');
//     div.innerHTML = user[field];
// }

//dataset
// чтение
// alert(order.dataset.orderState); // new
//
// // изменение
// order.dataset.orderState = "pending"; // (*)

// let div = document.querySelector('[data-widget-name]');
// alert(div.dataset.widgetName);
// alert(div.getAttribute('data-widget-name'));

// let links = document.querySelectorAll('a');
//
// for( let link of links){
//     let href = link.getAttribute('href');
//     if (!href) continue; // нет атрибута
//
//     if (!href.includes('://')) continue; // нет протокола
//
//     if (href.startsWith('http://internal.com')) continue; // внутренняя
//
//     link.style.color =  'orange';
// }

// let selector = 'a[href*="://"]:not([href^="http://internal.com"])';
// let links = document.querySelectorAll(selector);
//
// links.forEach(link => link.style.color = 'orange');
//
// // найти все ссылки, атрибут href у которых содержит ://
// // и при этом href не начинается с http://internal.com
