// for (let i = 0; i < document.body.children.length; i++) {
//     alert( document.body.children[i] ); // DIV, UL, DIV, SCRIPT
// }
//
// elem.firstElementChild === elem.children[0];
// elem.lastElementChild ===elem.children[elem.children.length-1];
//
// let table = document.body.children[0];
//
// alert( table.rows[0].cells[0].innerHTML ); // "один"

//
// Проверка существования детей
// важность: 5!!!!!!
// Придумайте самый короткий код для проверки, пуст ли элемент elem.
//
// «Пустой» – значит нет дочерних узлов, даже текстовых.
//
// if (/*...ваш код проверки elem... */) { узел elem пуст }
// Что написать в условии if ?
//
// if (!elem.firstChild){}
// if (!elem.lastChild){}
// if (elem.childNodes.length){}
//
// Также существует метод hasChildNodes, который позволяет вызовом elem.hasChildNodes()
// определить наличие детей. Он работает так же, как проверка elem.childNodes.length != 0.
//
// if (elem.hasChildNodes()){}
// if (elem.childNodes.length !== 0){}
//


// let elements = document.querySelectorAll('ul > li:last-child');
//
// for (let elem of elements) {
//     alert(elem.innerHTML); // "тест", "пройден"
// }


// // может быть любая коллекция вместо document.body.children
// for (let elem of document.body.children) {
//     if (elem.matches('a[href$="zip"]')) {
//         alert("Ссылка на архив: " + elem.href );
//     }
// }


// let chapter  = document.querySelector('.chapter');
//
// alert(chapter.closest('.book'));
// alert(chapter.closest('.contents'));
//
// alert(chapter.closest('h1'));

// получить все элементы div в документе
// let divs = document.getElementsByTagName('div');


// let inputs = table.getElementsByTagName('input');
//
// for( let input of inputs){
//     alert( input.value + ': ' + input.checked );
// }



// let form = document.getElementsByName('my-form')[0];
//
// let article = form.getElementsByClassName('article');
// alert(article.length);
// // ищем по имени атрибута
// let form = document.getElementsByName('my-form')[0];
//
// // ищем по классу внутри form
// let articles = form.getElementsByClassName('article');
// alert(articles.length); // 2, находим два элемента с классом article

//
// Как найти?…
//
// Таблицу с id="age-table".
//     Все элементы label внутри этой таблицы (их три).
// Первый td в этой таблице (со словом «Age»).
// Форму form с именем name="search".
//     Первый input в этой форме.
//     Последний input в этой форме.

// 1. Таблица с `id="age-table"`.
let table = document.getElementById('age-table')

// 2. Все label в этой таблице
table.getElementsByTagName('label')
// или
document.querySelectorAll('#age-table label')

// 3. Первый td в этой таблице
table.rows[0].cells[0]
// или
table.getElementsByTagName('td')[0]
// или
table.querySelector('td')

// 4. Форма с name="search"
// предполагаем, что есть только один элемент с таким name в документе
let form = document.getElementsByName('search')[0]
// или, именно форма:
document.querySelector('form[name="search"]')

// 5. Первый input в этой форме
form.getElementsByTagName('input')[0]
// или
form.querySelector('input')

// 6. Последний input в этой форме
let inputs = form.querySelectorAll('input') // найти все input
inputs[inputs.length-1] // взять последний