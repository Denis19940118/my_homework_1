// const element = document.querySelector('.child');

// const highlight = () => {
//   element.classList.toggle('highlighted');
//   // element.style.backgroundColor = `rgb(${Math.floor(Math.random() * 256)}, ${Math.floor(Math.random() * 256)}, ${Math.floor(Math.random() * 256)})`;
//   // if(!element.style.backgroundColor) {
//   // }
//   // else {
//   //   element.style.backgroundColor = '';
//   // }
// };
//
// const move = () => {
//   element.style.marginTop = (parseInt(element.style.marginTop || 0) + 10) + 'px';
// };
//
// element.addEventListener('click', highlight);

// element.addEventListener('click', move);

// element.remove();

// element.removeEventListener('click', highlight);
// element.removeEventListener('click', move);


// const el = {
//   _events: {}, // {click: [fn, fn2, ...]}
//   addEventListener(eventName, callback) {
//     let callbackList = this._events[eventName];
//     if(!callbackList) {
//       callbackList = [];
//     }
//     callbackList.push(callback);
//     this._events[eventName] = callbackList;
//   },
//   removeEventListener(eventName, callback) {
//     let callbackList = this._events[eventName];
//     const callbackIndex = callbackList.indexOf(callback);
//     if(!callbackList || callbackIndex === -1) {
//       return;
//     }
//     callbackList.splice(callbackIndex, 1);
//   }
// };


// const children = document.querySelectorAll('.child');
// // console.log(children);
// children.forEach((child) => {
//   child.addEventListener('click', fn);
//   child.addEventListener('mouseover', fn);
// });
// // element.addEventListener('click', function() {
// //   console.log(this);
// // });
//
// function fn(e) {
//   // console.log(this);
//   console.log(e.type);
//   // console.log(event.target); // element that event was fired on
//   // console.log(event.currentTarget); // element that event listener was attached to
//   // event.target.style.backgroundColor = 'green';
// }

// MouseEvent
// KeyboardEvent
// TouchEvent
// Event

// mousedown
// mouseup
// mouseover
// mouseout
// mouseenter
// mouseleave
// mousemove
// click
// contextmenu

// const child = document.querySelector('.child');
//
// const fn = event => {
//   // if(event.target === child) {
//     if(event.type === 'mouseenter') {
//       child.classList.add('highlighted');
//     }
//     else {
//       child.classList.remove('highlighted');
//     }
//   // }
// };
//
// child.addEventListener('mouseenter', fn);
// child.addEventListener('mouseleave', fn);
//
// child.addEventListener('contextmenu', e => {
//   // e.preventDefault()
//   console.log(e)
// })
//
//
//

// const child = document.querySelector('.child');
// child.addEventListener('click', onClickWrapper);
//
// function onClickWrapper({offsetX, currentTarget}) {
//   const width = currentTarget.offsetWidth;
//   let ratio = offsetX / width;
//   ratio *= 2; // 0...1...2
//   //             0...1...0
//   // 1...2 => 1...0 -> 2-ratio
//   // 1...0
//   if(ratio > 1) {
//     ratio = 2 - ratio;
//   }
//   currentTarget.style.background = `rgba(0,255,0,${ratio * 2})`;
//
// }


const child = document.querySelector('.child');
child.style.position = 'absolute';
document.addEventListener('mousemove', onClick);

function onClick({pageX, pageY}) {
    child.style.margin = '0';
    child.style.top = `${pageY - child.offsetHeight / 2}px`;
    child.style.left = `${pageX - child.offsetWidth / 2}px`;
}
