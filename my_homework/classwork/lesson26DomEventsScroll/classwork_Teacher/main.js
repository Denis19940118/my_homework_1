// // document.querySelector('.parent').addEventListener('scroll', evt => {
// //
// // });
//
// // debounce, throttle
//
// // mousewheel
// window.addEventListener('resize', () => {
//   setTimeout(() => {
//     // complex task
//   }, 100)
// });
//


// window.scrollY
//
// const children = document.querySelectorAll('.child');
//
// document.addEventListener('scroll', onScroll);
// // onScroll();
//
// function onScroll() {
//   children.forEach(child => {
//     const {top, bottom} = child.getBoundingClientRect();
//     if(bottom > 100 && top < window.innerHeight - 100) {
//       child.classList.add('appeared');
//     }
//     else {
//       child.classList.remove('appeared');
//     }
//     // child.classList[bottom > 0 && top < window.innerHeight ? 'add' : 'remove']('appeared');
//   })
// }


// options up down
// unfocus on enter
// mask

// document.addEventListener('keydown', ({shiftKey, code}) => {
//   if(code === "ArrowRight" && shiftKey) {
//     alert('...')
//   }
// });

// let shiftKey = false;
// document.addEventListener('keydown', evt => {
//   if(evt.keyCode === 16) {
//     console.log('shift down')
//     shiftKey = true;
//   }
//   if(evt.keyCode === 39 && shiftKey) {
//     console.log('arrow with shift');
//     alert(1234)
//   }
// })
// document.addEventListener('keyup', evt => {
//   if(evt.keyCode === 16) {
//     console.log('shift up')
//     shiftKey = false;
//   }
// })


document.querySelectorAll('.select').forEach(select => {
    const options = select.querySelector('.options');
    const valueEl = select.querySelector('.value');
    let active = options.firstElementChild;
    active.classList.add('active');

    select.addEventListener('keydown', ({code}) => {
        if (code === 'Space') {
            openOptions();
        }
        if (code === 'Escape') {
            closeOptions();
        }
        if(code === 'ArrowDown') {
            highlightNextOption();
        }
        if(code === 'ArrowUp') {
            highlightPrevOption();
        }
        if(code === 'Enter') {
            choose();
            closeOptions();
        }
    });

    function choose() {
        valueEl.innerText = active.innerText;
    }

    function highlightPrevOption() {
        active.classList.remove('active');
        active = active.previousElementSibling;
        if(!active) {
            active = options.lastElementChild;
        }
        active.classList.add('active');
    }

    function highlightNextOption() {
        active.classList.remove('active');
        active = active.nextElementSibling;
        if(!active) {
            active = options.firstElementChild;
        }
        active.classList.add('active');
    }

    function openOptions() {
        options.classList.add('opened');
        // options.firstElementChild.classList.add('active');
    }

    function closeOptions() {
        options.classList.remove('opened');
        // options.querySelector('.active').classList.remove('active');
    }

    select.addEventListener('blur', evt => {
        closeOptions();
    });
});

// document.querySelector('input').addEventListener('keydown', ({code, target}) => {
//   if(code === 'Enter') {
//     target.blur();
//   }
// })
