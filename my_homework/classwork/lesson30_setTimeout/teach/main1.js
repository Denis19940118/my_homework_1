// const timeEl = document.querySelector('time');
//
// const now = new Date();
// let h = now.getHours(),
//   m = now.getMinutes(),
//   s = now.getSeconds();
//
// const updateRate = 1;
//
// renderTime();
// console.time('set interval');
// setInterval(renderTime, updateRate * 1000);
// console.timeEnd('set interval');
//
// function renderTime() {
//   s += updateRate;
//   if (s >= 60) {
//     m++;
//     s = 0;
//   }
//   if (m >= 60) {
//     m = 0;
//     h++;
//   }
//   if (h >= 24) {
//     h = 0;
//   }
//   timeEl.innerText = `${h}:${m}:${s}`;
// }
//
// // 00:00:00.000   500 // setInterval
// // 00:00:00.500   500 // for 2s
// // 00:00:01.000   500
// // 00:00:01.500   500
// // 00:00:02.000   500
// ------ for finished
// // 00:00:02.500   500
// // 00:00:03.000   500
// // 00:00:03.500   500
//
// event loop

/*
* setTimeout 500
* for 2s
* ...2s
* move
* setTimeout 500
*
* */

//
// run();
// setInterval(run, 500); // 00:00:00.000
// setTimeout(() => {
//   for (let i = 0; i < 8888888888; i++) {
//   }
// });
//
// function run() {
//   const now = new Date();
//   console.log(now.getSeconds(), now.getMilliseconds());
// }

// run();
// setTimeout(run, 500); // 00:00:00.000 00:00:00.500
// setTimeout(() => {
//   console.time('for');
//   for (let i = 0; i < 488888888; i++) {
//   }
//   console.timeEnd('for');
// }, 480);  // 00:00:00.480
// //00:00:00.880
//
// function run() {
//   const now = new Date();
//   console.log(now.getSeconds(), now.getMilliseconds());
// }


const timeEl = document.querySelector('time');
let count = 0;

console.time('time');
print();

function print() {
  if(count % 1000 === 0) {
    console.timeEnd('time');
    console.time('time');
    timeEl.innerText = count / 1000; // 0
  }
  count+=50;
  setTimeout(print, 50); // 1100
}

// const boxEl = document.querySelector('.box');
// // let x = 0;
// let count = 0;
// let startTime = Date.now(); // performance.now()
// console.time('move');
// // move();
// let id = setInterval(move, 10);
//
// function move() {
//   console.timeEnd('move');
//   count++;
//   const now = Date.now();
//   const timeDiff = now - startTime;
//   // x++;
//   boxEl.style.transform = `translate3d(${timeDiff / 10}px, 0, 0)`;
//   console.time('move');
//   if(timeDiff <= 5000) {
//     // setTimeout(move, 10); 10+
//   }
//   else {
//     clearInterval(id);
//     console.log('count', count)
//   }
// }
//
// const button = document.querySelector('button');
// button.addEventListener('click', () => {
//   for(let i = 0; i < 999999999; i++){}
// });

// let count = 1;
// console.time('interval');
//
// const id = setInterval(function() { // 00:00:00.000
//     console.timeEnd('interval');
//
//     count++;
//     const now = new Date();
//     console.log(now.getSeconds(), now.getMilliseconds());
//     if(count > 5) {
//         clearInterval(id);
//     }
//
//     console.time('interval')
// }, 200);
//
// console.time('for');
// for(let i = 0; i < 999999999; i++){} // 500
// console.timeEnd('for');

