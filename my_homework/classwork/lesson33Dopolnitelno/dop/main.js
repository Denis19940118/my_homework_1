class Tree {
    constructor(name) {
        this.name = name;
    }
    grow(){
        console.log(`${this.name} is growing!`)
    }
}

class Beryoza extends Tree {
    growBeryoza() {
        console.log("Beryoza is growing!");
    }
}

class Oak extends Beryoza{

}


const tree = new Tree('Redroom');
const beryoza = new Beryoza('Beryoza');
const oak = new Oak('oak'); 

console.log(tree);
console.log(beryoza);
console.log(oak);

beryoza.grow();
oak.growBeryoza();