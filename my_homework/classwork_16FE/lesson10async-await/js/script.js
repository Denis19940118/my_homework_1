// new Promise((resolve, reject) => {
//     resolve('done')
// })
// .then(data => {
//     console.log(data);
// })
// .catch((error) => console.error(error))
// .finally(r => console.log('any way'));

// const responsePromise = fetch();
// console.log(responsePromise);

// async function showMessage(msg) {
// 	const time = await setTimeout(() => {
// 		document.write(msg);
// 	}, 2000);

// 	console.log('set time out', time);
// }
// console.log(showMessage('gogi'))

async function showMessage(msg) {
	const items = await fetch('https://swapi.dev/api/people');
	console.log('after fetch');
	console.log('set time out', items);
}
console.log(showMessage('gogi'));
