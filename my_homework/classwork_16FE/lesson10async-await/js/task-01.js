/** ЗАДАЧА - 1
 * Реализовать страницу новостной ленты используя синтаксис async await
 *
 * Отправить GET запрос для получения постов на:
 * @link https://ajax.test-danit.com
 *
 * Получим массив всех постов.
 * Для размещения на странице постов создать класс Post, которые дублирует все свойства объектов в ответе.
 * Так же в этом классе в конструкторе:
 *  - должны объявляться DOM элементы и записываться в качестве свойств.
 *  - создать метод render('.parent-elem-class'), который будет принимать 1 аргумент - селектор, для того чтобы найти родительский элемент, куда нужно будет вставить элемент. Ответственность метода render() - разместить элемент на странице.
 *
 * Нужно отобразить на странице все эти посты в виде карточек.
 */
const API = 'https://ajax.test-danit.com/api/json';
class Post {
	constructor({ id, userId, title, body }) {
		this.id = id;
		this.userId = userId;
		this.title = title;
		this.body = body;

		this.elements = {
			userId: document.createElement('p'),
			title: document.createElement('p'),
			body: document.createElement('p'),
		};

		const { userId: user, title: head, body: text } = this.elements;
		user.classList.add('post__user');
		head.classList.add('post__title');
		text.classList.add('post__text');
	}

	render(selector) {
		const targetEl = document.querySelector(selector);
		this.divEl = document.createElement('div');
		const { userId, title, body } = this.elements;

		this.divEl.classList.add('post');
		this.divEl.dataset.id = this.id;

		const btn = document.createElement('button');
		btn.innerText = 'See comments';
		btn.addEventListener('click', () => {
			this.onSeeComments();
		});

		this.getAutorName().then(name => (userId.innerText = name));

		title.innerText = this.title;
		body.innerText = this.body;

		this.divEl.append(userId, title, body, btn);
		targetEl.append(this.divEl);
	}

	async getComments() {
		const response = await fetch(`${API}/posts/${this.userId}/comments`);
		return response.json();
	}

	onSeeComments() {
		this.getComments().then(comments => {
			comments.forEach(comment => {
				const c = new Comment(comment);
				c.render(`.post[data-id="${this.id}"]`);
			});
		});
	}

	async getAutorName() {
		const resp = await fetch(`${API}/users/${this.userId}`, {
			headers: {
				'Access-Control-Allow-Origin': '*',
			},
		});
		return resp.json().then(u => u.name);
	}
}

const post = new Post({
	id: 1,
	userId: 4,
	title: 'Lorem*4',
	body: 'lkjgfdtuoyufkfhjkfjkfkfyukh',
});

// post.render('#postsPoot');

const getPosts = async () => {
	const response = await fetch(`${API}/posts`);
	return response.json();
};

console.log('Posts: ', getPosts());
getPosts().then(data =>
	data.forEach(item => {
		const post = new Post(item);
		post.render('#postsPoot');
	})
);

// const API = 'https://ajax.test-danit.com/api/json';

// class Post {
// 	constructor({ id, userId, title, body }) {
// 		this.id = id;
// 		this.userId = userId;
// 		this.title = title;
// 		this.body = body;
// 		this.elements = {
// 			userId: document.createElement('p'),
// 			title: document.createElement('p'),
// 			body: document.createElement('p'),
// 		};

// 		const { userId: user, title: head, body: text } = this.elements;
// 		user.classList.add('post__user');
// 		head.classList.add('post__title');
// 		text.classList.add('post__text');
// 	}

// 	async getAuthorName() {
// 		const resp = await fetch(`${API}/users/${this.userId}`, {
// 			headers: {
// 				'Access-Control-Allow-Origin': '*',
// 			},
// 		});
// 		return resp.json().then(u => u.name);
// 	}

// 	render(selector) {
// 		const targetEl = document.querySelector(selector);
// 		this.divEl = document.createElement('div');
// 		const { userId, title, body } = this.elements;

// 		this.divEl.classList.add('post');
// 		this.divEl.dataset.id = this.id;

// 		const btn = document.createElement('button');
// 		btn.innerText = 'See comments';
// 		btn.addEventListener('click', () => {
// 			this.onSeeComments();
// 		});

// 		this.getAuthorName().then(name => (userId.innerText = name));
// 		title.innerText = this.title;
// 		body.innerText = this.body;

// 		this.divEl.append(userId, title, body, btn);
// 		targetEl.append(this.divEl);
// 	}

// 	async getComments() {
// 		const response = await fetch(
// 			`https://ajax.test-danit.com/api/json/posts/${this.userId}/comments`
// 		);
// 		return response.json();
// 	}

// 	onSeeComments() {
// 		this.getComments().then(comments => {
// 			comments.forEach(comment => {
// 				const c = new Comment(comment);
// 				c.render(`.post[data-id="${this.id}"]`); //.post:nth-child()
// 			});
// 		});
// 	}
// }

// const post = new Post({
// 	id: 1,
// 	userId: 5,
// 	title: 'some title',
// 	body: 'sdfghm,mndfghjklkkj',
// });

// // post.render('#postsPoot');

// const getPosts = async () => {
// 	const response = await fetch(`${API}/posts`);
// 	return response.json();
// };

// getPosts().then(data =>
// 	data.forEach(item => {
// 		const post = new Post(item);
// 		post.render('#postsPoot');
// 	})
// );
