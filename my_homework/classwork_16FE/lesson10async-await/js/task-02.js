/** ЗАДАЧА - 2
 * Добавить к каждому посту кнопку "See comments"
 *
 * Для этого в классе Post добавить метод onSeeComments
 * Он должен быть вызван в обработчике клика по кнопке "See comments"
 * Внутри этого метода отправляется запрос на получение комментариев этого конкретного поста.
 * Для начала выводим полученные комментарии в консоль.
 *
 * Создаем класс Comment созданный по принципу класса Post из предыдущего задания.
 */
class Comment {
	constructor({ name, body, email }) {
		this.name = name;
		this.body = body;
		this.email = email;

		this.elements = {
			name: document.createElement('p'),
			body: document.createElement('p'),
			email: document.createElement('p'),
		};
	}

	render(sel) {
		const targetEl = document.querySelector(sel);
		const divEl = document.createElement('div');
		const { name, body, email } = this.elements;

		name.innerText = this.name;
		body.innerText = this.body;
		email.innerText = this.email;

		divEl.append(name, body, email);
		targetEl.append(divEl);
	}
}

const comment = new Comment({
	name: 'Gogi',
	body: 'Gogiffffffffffffffffffffffffff',
	email: 'Gogi@gmail.com',
});

console.log(comment);
