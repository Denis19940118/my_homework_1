const TABLES = {
	posts: 'posts',
	comments: 'comments',
	albums: 'albums',
	photos: 'photos',
	todos: 'todos',
	users: 'users',
};

const BASE_PATH = 'https://jsonplaceholder.typicode.com/';

export { TABLES, BASE_PATH as PATH };

function add(num, num2) {
	return num1 + num2;
}

function addTwice(num, num2) {
	return (num1 + num2) * 2;
}

function calc(x, y, callback) {
	callback(x, y);
}

calc(1, 2, add);
calc(1, 2, addTwice);
