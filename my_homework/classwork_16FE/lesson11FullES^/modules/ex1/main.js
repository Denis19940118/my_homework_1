import inputType from './inputType.js';
import Input from './Input.js';

const root = document.getElementById('root');

const input1 = new Input({
	name: 'login',
	type: inputType.text,
	required: true,
	className: 'login red',
	placeholder: 'Enter you login',
	errorText: 'Something went wrong',
});

const input2 = new Input({
	name: 'password',
	type: inputType.password,

	className: 'password green',
	placeholder: 'Enter you password',
	errorText: ' wrong password',
});

root.innerHTML = `~${input1.render()}${input2.render()}`;

input1.on('click', () => {
	console.log('Olalala');
});
input2.on('click', () => {
	console.log('osdkgskjgs');
});
input1.once('click', () => {
	console.log("I've worked only once");
});
