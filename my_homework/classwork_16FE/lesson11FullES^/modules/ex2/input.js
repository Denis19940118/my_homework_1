class Input {
	constructor({
		type,
		name,
		required = false,
		className,
		placeholder,
		errorText,
	}) {
		this.type = type;
		this.name = name;
		this.required = required;
		this.className = className;
		this.placeholder = placeholder;
		this.errorText = errorText;
		this.elem = null;
		this.id = Date.now();
	}
	render() {
		return `
        <input
        type = "${this.type}"
        name = "${this.name}"
        class = "${this.classNam}"
        placeholder = "${this.placeholder}"
        data-errorMsg = "${this.errorText}"
        id = "${this.id}"
        />`;
	}

	on(eventName, callBack) {
		const elem = findElem();

		if (elem) {
			elem.addEventListener(eventName, callBack);
		}
	}
	off(eventName, callBack) {
		const elem = findElem();

		if (elem) {
			elem.addEventListener(eventName, callBack);
		}
	}
	once(eventName, callBack) {
		const elem = findElem();

		if (elem) {
			elem.addEventListener(eventName, callBack);
		}
	}
	findElem() {
		return document.getElementById(this.id);
	}
}
