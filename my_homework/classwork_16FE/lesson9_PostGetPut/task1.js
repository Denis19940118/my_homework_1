/* ЗАДАЧА 1
 * Изучить документацию сервера по адресу:
 * @link - https://192.168.10.72:9003/
 *
 * Отправить GET запрос на сервер для получения всех пользователей.
 *
 * Преобразовать данные ответа в JavaScript объекты карточек пользователей.
 *
 * Разместить на странице карточки пользователей, которые будут содержать следующую информацию:
 *  - имя пользователя
 *  - никнейм
 *  - эл.почта
 *  - телефон
 *  - сайт
 *  - кнопка "See posts"
 * */
const API = 'https://ajax.test-danit.com/api/json';
const task1 = fetch(`${API}/users/`, {
	method: 'GET',
})
	// .then(response => response.json())
	.then(response => {
		return response.json();
	})
	// .then(json => {
	// 	const ul = document.createElement('ul');

	// 	document.body.append(ul);

	// 	for (let [key, value] of Object.entries(json)) {
	// 		const li = document.createElement('li');
	// 		ul.append(li);
	// 		const keyy = document.createElement('p');
	// 		keyy.innerText = key;
	// 		const valuee = document.createElement('strong');
	// 		valuee.innerText = value;
	// 		li.append(keyy, valuee);
	// 	}

	// 	console.log(json);
	// });
	.then(json => {
		console.log(json);
		json.forEach(createUser);
	});
// const createPost = document.createElement('button');
// createPost.id = 'postBoom';
// createPost.innerText = 'posts user';
// createPost.addEventListener('click', () => {
// 	if (json.id === posts.userId) {
// 		posts.forEach(data => {
// 			const title = document.createElement('p');
// 			const body = document.createElement('p');
// 			title.classList.add('card__info-item');
// 			body.classList.add('card__info-item');
// 			title.innerText = data.title;
// 			body.innerText = data.body;
// 		});
// 	}
// });
// document.querySelector('.users').append(createPost);

function createUser(data) {
	const card = document.createElement('div');
	card.classList.add('card');
	const name = document.createElement('p');
	const nickName = document.createElement('p');
	const mail = document.createElement('p');
	const phone = document.createElement('p');
	const site = document.createElement('p');

	const createPost = document.createElement('button');
	// createPost.id = 'postBoom';
	createPost.innerText = 'posts user';

	createPost.addEventListener('click', event => {
		fetch(`${API}/posts?userId=${data.id}`)
			.then(response => response.json())
			.then(json => console.log(json));
	});

	name.classList.add('card__info-item');
	nickName.classList.add('card__info-item');
	mail.classList.add('card__info-item');
	phone.classList.add('card__info-item');
	site.classList.add('card__info-item');

	name.innerText = data.name;
	nickName.innerText = data.username;
	mail.innerText = data.email;
	phone.innerText = data.phone;
	site.innerText = data.website;

	card.append(name, nickName, mail, phone, site, createPost);

	document.querySelector('.users').append(card);
}

// div.classList.add('user');
// nameP.classList.add('user__info-item');
// nickP.classList.add('user__info-item');
// mailP.classList.add('user__info-item');
// phoneP.classList.add('user__info-item');
// siteP.classList.add('user__info-item');

// nameP.innerText = item.name;
// nickP.innerText = item.username;
// mailP.innerText = item.email;
// phoneP.innerText = item.phone;
// siteP.innerText = item.website;

// div.append(nameP, nickP, mailP, phoneP, siteP, btnUserPosts);
