/* ЗАДАЧА 2
 * К коду предыдущего задания добавить возможность создать пользователя.
 *
 * Добавить кнопку "New user" в верхнем правом углу. По нажатию на нее:
 *  - показать для заполнения форму создания нового пользователя
 *  - после нажатия кнопки "Save" в форме должен отправляться POST запрос на сервер для создания пользователя.
 *  - с сервера вернутся в качестве ответа новый объект со всеми данными из формы И ID
 *  - форма должна пропасть после успешного ответа
 *  - затем на экране должна появиться карточка нового пользователя
 * */

const newUser = document.createElement('button');
const buttonContainer = document.querySelector('.button-container');

buttonContainer.prepend(newUser);
newUser.classList.add('new-btnUser');
newUser.innerText = 'New user';

newUser.onclick = () => {
	const form = document.querySelector('.new-user');
	form.classList.add('df');
};

const getFormData = () => {
	const formEl = document.querySelector('.new-user');
	const data = new FormData(formEl);

	return {
		name: data.get('name'),
		nickName: data.get('nickName'),
		email: data.get('email'),
		phone: data.get('phone'),
		site: data.get('site'),
	};
};

const saveUser = newUser => {
	const url = `${API}/users`;
	const options = {
		method: 'POST',
		body: JSON.stringify(newUser),
	};

	fetch(url, options)
		.then(response => response.json())
		.then(data => {
			createUser(data);
			const form = document.querySelector('.new-user');
			form.reset();
			form.classList.remove('df');
		});
};

document.querySelector('.new-user').addEventListener('submit', event => {
	event.preventDefault();
	const formData = getFormData();
	saveUser(formData);
});
