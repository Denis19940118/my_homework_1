// const user = {
//     name: 'My name',
//     getName() {
//         return this.name;
//     }
// };
// // const user2 = {
// //     name: 'My name',
// //     getName() {
// //         return this.name;
// //     }
// // };
// const user3 = {
//     name: 'My name',
//     getName() {
//         return this.name;
//     }
// };
// console.log(this.getName());
// // console.log(user.[getName()]);
//
// // const{getName} = user;
//
// user2.getName = user.getName;
// console.log(user2.getName());
//
// console.log(getName());
// // console.log([getName()]);

// const user = {
//     name: 'Test',
//     getName: () => {
//         return this.name;
//     }
// };
//
//
// var name = 'WindowName';
//
// const user2 = {
//     name: 'Other',
// };
//
// user2.getName = user.getName;
// console.log(user2.getName());

// const user = {
//     name: 'Test',
//     getNameFunc() {
//         return () => this.name;
//     }
// }

// console.log(user.getNameFunc()());
//
//
// const listComponent = {
//     list: [],
//     loading: true,
//     loadFilms(onLoaded) {
//         this.loading = true;
//         // setTimeout(function (response) {
//         //     this.list = response// this = window это плохо
//         //
//         // });
//         setTimeout((response) => {
//             this.list = ["Film1", "Film2"];
//             this.loading = false;
//             onLoaded();
//         }, 1000);
//     }
// }
//
// renderFilms(listComponent.list);
// listComponent.loadFilms(() => {
//     renderFilms(listComponent.list)
// });

// function renderFilms(list) {
//     console.log(list)
// }

// const audio = {
//     element: document.querySelector('audio'),
//     loaded: false,
//     init() {
//         this.element.addEventListener('...');
//         this.loaded = true;
//     }
// }


// ==============================(call) работвет только с декларативной функцией===========================================
// const func = function(suffix) {
// return this.name + " " + suffix;
// }
// const func = (suffix) => {
//     return this.name + " " + suffix;
// };
// const user = {
//     name: 'Test',
// }
//
// console.log(func.call(user));
//
// console.log(func.call(user, 'suff'));
// console.log(func.apply(user, ['suff']));
// const positive = [1,2,3,41,-1].filter(el=>el>0)
// // console.log(Math.min.apply(null, positive));
// console.log(Math.min.(...positive));


// const boundFunc = func.bind({name: "bound name"});
// console.log(boundFunc());
//
// const user = {
//     name: "Anonimous",
//     log() {
//         console.log(this.name);
//     }
// }
//
// document.querySelector('button').addEventListener('click', user.log.bind(user));
// // user.log();
// // setInterval(function () {
// //      user.log();
// // }, 1000)
// // setInterval( user.log, 1000);
// // const log = user.log.bind(user);
// // log();
// // setInterval( user.log.bind(user), 1000);
//
// Function.prototype.bind2 = function (context) {
//      //this = function()
// //     const originalFunction = this;
// //     return function () {
// // return originalFunction.call(context);
// //     }
// //     return  () => {
// //         return this.call(context);
// //     }
//     return  (...args) => {
//         return this.apply(context, args);
//     }
// };
// // calc.bind2();
//
// const calc2 = calc.bind2({age: 12}, 8,1, 4);
// // calc2();
// console.log(calc2(3));
//
// function calc(...args) {
//     console.log(args);
// console.log(this.age);
//     return this.age + args[0];
// }

// function setInterval(callback, delay) {
// ///.....
//     callback();
// }


// const user = {
//     first:"First name",
//     second: "Second name"
// }

// const app = {
//     user: {
//         first: "First name",
//         second: "Second name"
//     },
//     handleUserChange(property, event) {
//     this.user[property] = event.target.value;
//     console.log(this.user);
//     }
// }
//         console.log(app);
// // function handleUserChange (property , event) {
// //     user[property] = event.target.value;
// //     console.log(user);
// // }
// const  f = document.querySelector('#f');
// const  s = document.querySelector('#s');
// f.addEventListener('input', app.handleUserChange.bind(app, 'first'));
// s.addEventListener('input', app.handleUserChange.bind(app, 'second'));

//================================ЗАМЫКАНИЕ================================


// function f(a) {
//     return (b) => {
//         return a + b;
//     }
// }


// console.log(f(3)(4));
// const f2 = f(3);
// console.log(f2(4));// 7
// console.log(f2(8));// 11

//
// for (let i = 1 ; i < 40; i++) {
//     const start = performance.now();
//     console.log(i, fibo1(i),performance.now-start);
// }
// // function fibo1(n) {
// //     if (n<3) {
// //         return 1;
// //     }
// //
// //     return fibo(n - 1) + fibo(n - 2);
// // }
// // // fibo(6);
//
// // const cache = {1: 1, 2: 1};
// //
// // function fibo2(n) {
// //     if (cache[n]) {
// //         return cache[n];
// //     }
// //     const res = fibo2(n - 1) + fibo2(n - 2);
// //
// //     cache[n] = res;
// //
// //     return res;
// // }
//
//
// const fibo2 =(() => {
//     const cache = {1: 1, 2: 1};
//     return(n) => {
//         if (cache[n]) {
//             return cache[n];
//         }
//         const res = fibo2(n - 1) + fibo2(n - 2);
//
//         cache[n] = res;
//
//         return res;
//     }
// })();


// function createGame(params) {
//     return (width, height) => {
//         //render
//         //params, fps
//     }
// }
//
// const game = createGame({fps: 1000, players: 4});
//
// render(300, 400);


function sum(a, b, c) {
    return a + b + c;
}

function curry(original) {

    let args = [];
    const collect = (...partialArgs) => {
        // args = args.concat(partialArgs);
        args = [...args, ...partialArgs];

        if (args.length >= original.length) {
            return original(...args);
        }
        return collect;
    }
    return collect;
    // original(...args); карирование 
}

const sumCurried = curry(sum);

console.log(sum.length);
// console.log(sumCurried(3)(4)(5));
// console.log(sumCurried(3,4)(5));
// console.log(sumCurried(3)(4,5));
// console.log(sumCurried(3,4,5));