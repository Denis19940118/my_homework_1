//HOISTING = ВСПЛЫТИЕ ,БРАУЗЕР В НАЧАЛЕ СВОЕЙ РАБОТЫ ПОДНИМАЕТ ВСЕ ФУНКЦИИ ДЕЛАРЕЙШЕН И ПЕРЕМЕННЫЕ ВАР В САМЫЙ ВВЕРХ,
// глобальная зона видемости

// function name () { //lexical scope {let and const}
//     let c = 3;
//     console.log(name);
//
//     name();
// }
//
// const name2 = function () {
// console.log(name2);
// }
//
// name();

// function a() {//{o = 1 }
//     let o = 1;
//     console.log(o);
//
//     function b() {
//         c();
//         console.log(o);
//
//         function c() {
//             let o = 3;
//
//             console.log(o);
//         }
//     }
//     b();
//
// }
// a();
//
//
//
//
// function c() { //{a: 2}
// let a = 2;
// d();
// }
//
// function d() {
// console.log(a);
// }


// let a = 1;
// func();
// a++;
// func();
// a++;
// func();
//
// function func() {
// console.log(a)
// }


// // {a: 1, a: 3, a: 4}
// let a = 1;
// func();
// a++;
// setTimeout(func, 1);
// a++;
// func();
//
// function func() {
//     console.log(a)
// }
//
//
// // 1,3,4


// for (var i = 0; i < 10; i++){
//     setTimeout(() => console.log(i),0)
// }

// for (var  i = 0; i < 10; i++){
//     //iife = immediately invoked function expression
//     ((a) => {
//     setTimeout(() => console.log(a),1000)//замыкание, функция которая использует чтото с наружи(знфчение с наружи)
//     })(i);
//     // (function a() {
//     // setTimeout(() => console.log(i),1000)})
// }


// for (var i = 0; i < 10 ; i++){
//     (function _loop(i) {
//         setTimeout(function () {
//             return console.log(i);
//         }, 0);
//     })(i);
// }
//
// const a = () => {};
// const b = c => {};
// // let c: (d: {age: number}) => void;

// c = u => u.age++;
// let c = age => {age};
// // const  user = {age: 10};
// // console.log(c(user));
// // console.log(user);
//
//
// const name = 'Name';
// const age = 11;
// // const user = {name, age, prop};
// // do{
// //     const prop = prompt('Prop name: '); //hands
// //     const val = prompt('Val : '); //"2"
// //     user[prop] = val;
// // }while (confirm(Do));
//
// const prop = prompt('Prop name: '); //hands
//     const val = prompt('Val : '); //"2"
//     // user[prop] = val;
//     const otherUser = {name: 'other', legs: 2};


// const user = {...otherUser, name, age, [prop + "__" + (2 + 3)]: val};
// const user1 = { name, age, [prop + "__" + (2 + 3)]: val, ...otherUser};
// console.log(user);
// console.log(user1);

function createUser() {
    const name = 'Name';
    const age = 11;
    const prop = prompt('Prop name: '); // hands
    const val = prompt('Val: '); // "2"
    const otherUser = {name: "other", legs: 2};
    const user = {
        name,
        age,
        children: [{name: 'Child1', age: 10}, {name: 'Child2', age: 7}],
        [prop + "___" + (2 + 3)]: val,
        ...otherUser,
    };

}

const user = createUser();
// const name = user.name;
// const num = user.age;
// const secondChildAge = user.children[1].age;
// const {name, age: num,children: [,{age}]} = user;
// const {name, ...otherProps} = user;
// const a = [1,2,3];
// const[, {val}] = a;
// const a = {prop: {val: 1}};
// const {prop}
// console.log(user);


// function sum ({x= 0 ,y= 0} = {}, {x: x2 = 0, y: y2 = 0} = {}) {
//  return{
//      x: x + x2,
//      y: y + y2
//  }
// }
//
// console.log(sym({x:1, y:2}), null)
//     // ({x: 3, y:-5}))
//
// //{x: 4, y: -3}


// $.fn.slick = function ({delayTime = 0,}) {
//
// }
//
// $('.slider').slick({
//     delayTime: 300
// })


function sum(...points) {
    console.log(arguments);
    // return [...arguments].reduce(
    return points.reduce(
        // ({x,y}, {x: x1=0, y: y1=0}) => {return {x: x+x1, y: y + y1}}
        ({x, y}, {x: x1 = 0, y: y1 = 0}) => ({x: x + x1, y: y + y1})
        , {x: 0, y: 0}
        );
}

console.log(sum({x: 1}, {y: 2}, {x: 3, y: -1}, {y: -10}));


const z = (...args) => {
    console.log(...args);
}

// //jquery
// (function (globalObject) {
// let func = () => {
//
// }
// setTimeout(() => {
// button.addEventListener('click', func)
// })
// });
// let func = () => {
//     console.log('123');
// }
// $();


//
// function hunguoutUser({user: {avatarSrc, name}}) {
// return(<>
// <img src = {avatarSrc}>
//         <span>{name}</apan>
//     <>)
// }