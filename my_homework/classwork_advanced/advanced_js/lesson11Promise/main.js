// Promise
// Thenable

// wait(pending, resolve, rejected)

// const promise = new Promise(() => {
//
// });

// makeRequest('url1', (response) ={
//     makeRequest('url2', (response2) = {
//
// })
// })
//
// function makeRequest(url, onSuccess) {
//
// }

// const btnFirstClick = new Promise((resolve, reject) => {
//     resolve(1);
//     // const button =document.querySelector('button');
//     // button.addEventListener('click');
// });
//
// btnFirstClick.then(event => {
//     console.log(event)
// })

// class Promise {
//     _resolve = [];
//     _reject = [];
//     _resolved = false;
//     _rejected = false;
//     _val;
//
//     constructor(createFn) {
//
//         createFn ((val) =>  {
//             if(this._resolved || this._rejected) {
//             return;
//         }
//             this._resolved = true;
//             this._resolve.forEach(callback => callback(val));
//             this._resolve = [];
//             this._val = val
//         }, (val) => {
//             this._rejected = true;
//             this._reject.forEach(callback => callback(val));
//             this._reject = [];
//             this._val = val
//         });
//     }
//
//     then(onResolve, onReject) {
//         if(this._resolved) {
//             onResolve(this._val)
//         }
//         if(this._rejected) {
//             onReject(this._val)
//         }
//         else {
//             this._resolve.push(onResolve);
//             this._reject.push(onReject);
//         }
//
//     }
// }

// const promise = new Promise((resolve, reject) => {
//     // resolve( 1);// 1
//     // resolve( 2);// netu
//     // // const req = new XMLHttpRequest()
//     // //     req.open('...');
//     // // req.send();
//     // // req.onload = resolve;
//
//     const onClick = event => {
//         console.log('clicked');
//         clearTimeout(timeoutId);
//         resolve(event)
//     };
//
//     const button =document.querySelector('button');
//     button.addEventListener('click', onClick, {once: true});
//
//     const timeoutId = setTimeout(() => {
//         console.log('timeout');
//         button.removeEventListener('click', onClick);
//         reject(`Haven't clicked button within 5 secs`)
//     }, 5000);
// });

// const  promise = new Promise((res, rej) => {
//     setTimeout(() => {
//         res('abc');
//     },1000);
// })

// promise.then( result => {
//     console.log(result);
// }, err => {
//     console.log(err);
// });

// setTimeout( () => {
//     promise.then(res => console.log(), )
// })

// setTimeout(() => {
//     promise.then(
//         res => console.log('10 sec res' + res);
//         err => console.log('10 sec rej' + err);
//         // event => {
//         // console.log(event)
//     })
// }, 10000);

// promise.then(() => {}, err => {
//     console.error('err');
// })
// promise.catch(( err => {
//     console.error('err');
// })


// Promise.resolve(123).then(val => console.log(val));
// PromiseResolve(123).then(val => console.log(val));
//
// function PromiseResolve(val) {
// return new Promise (res => res(val));
// }

// function wait(ms) {
//     return new Promise( res => setTimeout(res, ms))
// }

// setTimeout(function () {
//     console.log(1)
//     setTimeout(function () {
//         console.log(1)
//         setTimeout(function () {
//             console.log(1)
//         },1000);
//     },1000);
// },1000);
//
// wait(1000)
//     .then(() => {
//         console.log(1);
//         return wait(1000);
//     })
//     .then(() => {
//         console.log(2);
//         return wait(1000);
//     })
//     .then(() => {
//         console.log(3);
//     });
//
// wait(1000)
//     .then(() => console.log(1))
//     .then(() => wait(1000))
//     .then(() => console.log(2))
//     .then(() => wait(1000))
//     .then(() => console.log(3))
//
// Promise.resolve(123)
// .then(val => val + 1)
// .then(val => val + 1)
// .then(val => val + 1)
// .then(val => val + 1)
// .then(val => val + 1)
// .then(val => console.log(val));
//
// let arr = [1]
// .map(val => val * 2)
// .map(val => val * 2)
// .map(val => val * 2);

// Promise.all()  //возвращает все
// Promise.all()   //возвращает один

// const time =  Date.now();
// Promise.all([wait(1000).then(() => 1000),
//     wait(100).then(() => 100),
//     wait(800).then(() =>{
//         throw new Error()
//     })
// ])
//     .then((results) => {
//         console.log(Date.now() - time,results);
//     },(err) =>{
//         console.log(Date.now() - time,err);
//
//     });

// const time =  Date.now();
// Promise.race([wait(1000).then(() => 1000),
//     wait(100).then(() => 100),
//     wait(800).then(() => 800)
// ])
//     .then((res) => {
//         console.log(Date.now() - time,res);
//     });


// Promise.all([
//     fetch(`https://jsonplaceholder.typicode.com/posts`),//.then(res => res.json()),
//     fetch(`https://jsonplaceholder.typicode.com/users`)//.then(res => res.json()),
// ])
//     // .then(responses => Promise.all(responses.map(response => response.json())))
//     .then(responses => {
//         const jsons = responses.map(response => response.json());
//         // return Promise.all([responses[0].json(), responses[1].json()]);
//         return Promise.all(jsons)
//     })
//         // console.log(res);
//         // return res;
//
//     .then(([posts, users]) => {
//         posts.forEach(({title, userId}) => {
//             const user = users.find(user => user.id === userId);
//             const p = document.createElement('p');
//             p.innerText = `${user.username}: ${title}`;
//             document.body.append(p);
//         })
//     });

//
// fetch(`https://jsonplaceholder.typicode.com/users`)
//     .then(response => response.json())
//     .then(users => {
//        // return Promise.all([
//        //     fetch('https://jsonplaceholder.typicode.com/posts')
//        //         .then(response => response.json())
//        //     Promise.resolve(usersl)
//        //     users
//        //     ])
//       return fetch('https://jsonplaceholder.typicode.com/posts')
//           .then(response => response.json())
//           .then(posts => ({posts, users}));
//     })
//     .then(({posts, users}) => {
//         console.log(posts, users);
// });


// Promise.all([
//     getPosts(),
//     getUsers()
// ])
//     // .then(responses => Promise.all(responses.map(response => response.json())))
//     // .then(responses => {
//     //     const jsons = responses.map(response => response.json());
//     //     // return Promise.all([responses[0].json(), responses[1].json()]);
//     //     return Promise.all(jsons)
//     // })
//         // console.log(res);
//         // return res;
//
//     .then(([posts, users]) => {
//         posts.forEach(({title, userId}) => {
//             const user = users.find(user => user.id === userId);
//             const p = document.createElement('p');
//             p.innerText = `${user.username}: ${title}`;
//             document.body.append(p);
//         })
//     });

// const container = document.querySelector('#posts');
// let posts, users = []
// getUsers().then(data => {
//     users = data;
//     if(posts) {
//         printPosts(posts, users)
//     }
// });
//
// getPosts().then(data => {
//     posts = data;
//   printPosts(posts, users);
// });
//
// function printPosts(posts, users = []) {
//     container.innerHTML = '';
//     posts.forEach(({title, userId}) => {
//         const user = users.find(user =>user.id === userId);
//         const p = document.createElement('p');
//         p.innerText = `${user ? user.username : '...'}: ${title}`;
//         document.body.append(p);
//     })
// }


// const container = document.querySelector('#posts');
//
// const postsPromise = getUsers();
//
// postsPromise.then(posts => {
//     printPosts(posts, [])
// });
//
// Promise.all([postsPromise, getUsers()])
//     .then(([posts, users]) => {
//         printPosts(posts, users);
//     });
//
// function printPosts(posts, users = []) {
//     container.innerHTML = '';
//     posts.forEach(({title, userId}) => {
//         const user = users.find(user => user.id === userId);
//         const p = document.createElement('p');
//         p.innerText = `${user ? user.username : '...'}: ${title}`;
//         document.body.append(p);
//     })
// }
//
//
// function getUsers() {
//     return wait(Math.random() * 3000)
//         // .then( fetch.bind(null, `https://jsonplaceholder.typicode.com/users`))
//         .then(() => fetch(`https://jsonplaceholder.typicode.com/users`))
//         .then(res => res.json());
// }
//
// function getPosts() {
//     return wait(Math.random() * 3000)
//         .then(() => fetch(`https://jsonplaceholder.typicode.com/posts`))
//         .then(res => res.json());
// }
//
// function wait(ms) {
// // return new Promise(res => setTimeout(() => res(), ms))
//     return new Promise(res => setTimeout(res, ms))
// }

// const ctrl = new AbortController();
//
// fetch('https://jsonplaceholder.typicode.com/users', {
//     signal: ctrl.signal
// })
//     .catch(() => {});//отловить ошибку промиса
//
// setTimeout(() => ctrl.abort(), 10);

// async function a() {
//     let response = await fetch('https://jsonplaceholder.typicode.com/users');
//     let data = await response.json();
// // .then(res => res.json())
// // .then(data => response = data);
//     console.log(data);
// }

// async function func() {
// try {
//     const responses = await
//     Promise.all([
//         fetch('https://jsonplaceholder.typicode.com/posts'),
//         fetch('https://jsonplaceholder.typicode.com/users')
// Promise.reject('eroororor')
//     ])
// }
// catch (err) {
//     console.error(err)
// }
// //     let postsResponse = await fetch('https://jsonplaceholder.typicode.com/posts');
// // let usersResponse = await fetch('https://jsonplaceholder.typicode.com/users');
//
//     //     // let data = await response.json();
//     //     console.log(data);
//     //     return data;
// }

// async function func() {
//         const responses = await Promise.all([
//             fetch('https://jsonplaceholder.typicode.com/posts'),
//             fetch('https://jsonplaceholder.typicode.com/users')
//             Promise.reject('eroororor')
//         ])
// }
// console.log(func());
// func();

// document.cookie = 'name=Test';
// document.cookie = 'surname=Surname';
// let cookies = document.cookie.split(';').map(keyValue => keyValue.split('=').map(str => str.trim()));
//     // .reduce((acc, [key,value]) => ({...acc, [key]:value}), {});
//
// // document.cookie.match(/\s*surname\s*=([^;]*)/);
//
// cookies = Object.fromEntries(cookies);
// console.log(cookies.name);
// fetch('./', {
//     credentials:'include',
// });








// thenable

// wait(pending), resolved, rejected

// callback hell
// makeRequest('url1', (response) => {
//     makeRequest('url2', (response2) => {
//         response, response2
//     });
// });
// function makeRequest(url, onSuccess) {
// //
// }

// class Promise {
//     _resolves = [];
//     _rejects = [];
//     _resolved = false;
//     _rejected = false;
//     _val;
//
//     constructor(createFn) {
//         createFn((val) => {
//             if (this._resolved || this._rejected) {
//                 return;
//             }
//             this._resolved = true;
//             this._resolves.forEach(callback => callback(val));
//             this._resolves = [];
//             this._val = val;
//         }, (val) => {
//             if (this._resolved || this._rejected) {
//                 return;
//             }
//             this._rejected = true;
//             this._rejects.forEach(callback => callback(val));
//             this._rejects = [];
//             this._val = val;
//         });
//     }
//
//     then(onResolve, onReject) {
//         if (this._resolved) {
//             onResolve(this._val);
//         } else if (this._rejected) {
//             onReject(this._val);
//         } else { // pending
//             this._resolves.push(onResolve);
//             this._rejects.push(onReject);
//         }
//     }
// }

// const promise = new Promise((resolve, reject) => {
//     // const req = new XMLHttpRequest();
//     // req.open('...');
//     // req.send();
//     // req.onload = resolve;
//
//     const onClick = event => {
//         console.log('clicked');
//         clearTimeout(timeoutId);
//         resolve(event);
//     };
//
//     const button = document.querySelector('button');
//     button.addEventListener('click', onClick, {once: true});
//
//     const timeoutId = setTimeout(() => {
//         console.log('timeout');
//         button.removeEventListener('click', onClick);
//         reject(`Haven't clicked button within 5 secs`);
//     }, 5000);
// });

// const promise = new Promise((res, rej) => {
//     setTimeout(() => res('abc'), 1000);
// })

// promise.then(result => {
//     console.log(result);
// }, err => {
//     console.log(err)
// });

// setTimeout(() => {
//     promise.then(res => console.log('in 2 sec', res), res => {})
// }, 2000);


// setTimeout(() => {
//     promise.then(
//         res => console.log('10 sec res', res),
//         err => console.log('10 sec rej', err)
//     );
// }, 10000);

// console.log(2);
//

// promise.then(() => {}, err => {
//     console.error('err');
// })
// promise.catch(err => {
//     console.error('err');
// })


// PromiseResolve(123).then(val => console.log(val));
//
// function PromiseResolve(val) {
//     return new Promise(res => res(val));
// }

function wait(ms) {
    // return new Promise(res => setTimeout(() => res(), ms));
    return new Promise(res => setTimeout(res, ms));
}

// setTimeout(function () {
//     console.log(1);
//     setTimeout(() => {
//         console.log(2);
//         setTimeout(() => {
//             console.log(3);
//         }, 1000)
//     }, 1000)
// }, 1000)
//
// wait(1000)
//     .then(() => console.log(1))
//     .then(() => wait(1000))
//     .then(() => console.log(2))
//     .then(() => wait(1000))
//     .then(() => console.log(3))

// Promise.resolve(123)
//     .then(val => val + 1)
//     .then(val => wait(1000))
//     .then(val => val + 2)
//     .then(val => val + 1)
//     .then(val => console.log(val))


// let arr = [1]
//     .map(val => val * 2)
//     .map(val => val * 2)
//     .map(val => val * 2);
//
// fetch('url')
//     .then(res => res.json())
//     .then(res => console.log(res))
//     .then(() => fetch('url2'))
//     .then(res => res.json())
//     .then(res2 => console.log(res2));

// const time = Date.now();
// Promise.all([
//     wait(1000).then(() => '1000a'),
//     wait(100).then(() => '100b'),
//     wait(800).then(() => {
//         throw new Error()
//     })
// ])
//     .then((results) => {
//         console.log(Date.now() - time, results);
//     }, (err) => {
//         console.log(Date.now() - time, err);
//     });

// const time = Date.now();
// Promise.race([
//     wait(1000).then(() => 1000),
//     wait(100).then(() => 100),
//     wait(800).then(() => {
//         throw new Error()
//     })
// ])
//     .then((res) => {
//         console.log(Date.now() - time, res);
//     }, (err) => {
//         console.log(Date.now() - time, err);
//     });
//
// Promise.all([
//     fetch('https://jsonplaceholder.typicode.com/posts'),//.then(res => res.json()),
//     fetch('https://jsonplaceholder.typicode.com/users')//.then(res => res.json()),
// ])
//     .then(responses => {
//         const jsons = responses.map(response => response.json());
//         // return Promise.all([responses[0].json(), responses[1].json()]);
//         return Promise.all(jsons);
//     })
//     .then(([posts, users]) => {
//         posts.forEach(({title, userId}) => {
//             const user = users.find(user => user.id === userId);
//             const p = document.createElement('p');
//             p.innerText = `${user?.username}: ${title}`;
//             document.body.append(p);
//         })
//     });
// fetch('https://jsonplaceholder.typicode.com/users')
//     .then(response => response.json())
//     .then(users => {
//         // return Promise.all([
//         //     fetch('https://jsonplaceholder.typicode.com/posts')
//         //         .then(response => response.json()),
//         //     // Promise.resolve(users)
//         //     users
//         // ])
//         return fetch('https://jsonplaceholder.typicode.com/posts')
//             .then(response => response.json())
//             .then(posts => ({posts, users}));
//     })
//     .then(({posts, users}) => {
//         console.log(posts, users);
//     })

// Promise.all([
//     getPosts(),
//     getUsers()
// ])
//     // .then(responses => {
//     //     const jsons = responses.map(response => response.json());
//     //     // return Promise.all([responses[0].json(), responses[1].json()]);
//     //     return Promise.all(jsons);
//     // })
//     .then(([posts, users]) => {
//         posts.forEach(({title, userId}) => {
//             const user = users.find(user => user.id === userId);
//             const p = document.createElement('p');
//             p.innerText = `${user.username}: ${title}`;
//             document.body.append(p);
//         })
//     });

// const container = document.querySelector('#posts');
// const postsPromise = getPosts();
// // 1s - users
// // 2s - posts
//
// Promise.all([postsPromise, getUsers()]); // [posts, users]
// postsPromise.then(posts => [posts, []]); // [posts, []]
//
// function printPosts(posts, users) {
//     container.innerHTML = '';
//     posts.forEach(({title, userId}) => {
//         const user = users.find(user => user.id === userId);
//         const p = document.createElement('p');
//         p.innerText = `${user ? user.username : '...'}: ${title}`;
//         container.append(p);
//     })
// }


// function one() {
//     return 1;
// }
//
// const arr = [1, 2, 3];
// const newArr = arr.map(one());

// function getUsers() {
//     return wait(2000)
//         // .then(fetch.bind(null, 'https://jsonplaceholder.typicode.com/users'))
//         .then(() => fetch('https://jsonplaceholder.typicode.com/users'))
//         .then(res => res.json())
// }
//
// function getPosts() {
//     return wait(1000)
//         .then(() => fetch('https://jsonplaceholder.typicode.com/posts'))
//         .then(res => res.json())
// }

// const ctrl = new AbortController();
//
// fetch('https://jsonplaceholder.typicode.com/posts', {
//     signal: ctrl.signal
// });
//
// setTimeout(() => ctrl.abort(), 10);

// async function func() {
//     // const responses = await Promise.all([
//     //     fetch('https://jsonplaceholder.typicode.com/posts'),
//     //     fetch('https://jsonplaceholder.typicode.com/users'),
//     //     // Promise.reject('errorororor')
//     // ]);
//     // const data = await Promise.all(responses.map(res => res.json()));
//     const postsResponse = await fetch('https://jsonplaceholder.typicode.com/posts');
//     const posts = await postsResponse.json();
//     const usersResponse = await fetch('https://jsonplaceholder.typicode.com/users');
//     const users = await usersResponse.json();
//     return [posts, users];
// }

// function func() {
//     return fetch('https://jsonplaceholder.typicode.com/posts')
//         .then(() => fetch('https://jsonplaceholder.typicode.com/users'))
//         .then(() => undefined);
// }

// //
// function func() {
//     return new Promise((res, rej) => {
//         //code
//         resolve(1);
//     });
// }

// func().then(result => console.log(result));


// document.cookie = 'name=Test'
// document.cookie = 'surname=Surname'
let cookies = document.cookie
    .split(';')
    .map(keyvalue => keyvalue.split('=').map(str => str.trim()))
// .reduce((acc, [key, value]) => ({...acc, [key]: value}), {})
cookies = Object.fromEntries(cookies);

// document.cookie.match(/\s*surname\s*=([^;]*)/)

console.log(cookies.name);
fetch('/', {
    // credentials: 'include',
});

// withCredentials: true

class Component {
    _container;

    constructor() {
        this.createElement();
    }

    createElement() {
    }

    appendTo(parent) {
        parent.append(this._container);
    }

    _removeListeners() {
    }

    destroy() {
        this._removeListeners();
        this._container.remove();
    }
}

class Card extends Component {
    constructor() {
        super();
        this._container = document.createElement('div');
        this._container.innerText = 'abc';
    }
}

class Modal extends Component {
    constructor(component) {
        super();
        this._component = component;
    }

    open() {
        this._container = document.createElement('div');
        this._container.classList.add('modal');
        this._component.appendTo(this._container);
        this.appendTo(document.body);
    }

    close() {
        this._component.destroy();
        this.destroy();
    }
}


class FormControl extends Component {
    constructor(initialValue) {
        super();
        this.setValue(initialValue);
    }

    setValue(val) {
        this._container.value = val || '';
    }

    getValue() {
        return this._container.value;
    }
}

class InputComponent extends FormControl {
    constructor() {
        super();
    }
    createElement() {
        this._container = document.createElement('input');
    }
}

class CheckboxComponent extends FormControl {
    constructor(props) {
        super(props);
    }
    createElement() {
        this._container = document.createElement('input');
        this._container.type = 'checkbox';
    }

    getValue() {
        return this._container.checked;
    }
}

function controlFactory({type, initialValue}) {
    if(type === 'text') {
        return new InputComponent(initialValue);
    }
    if(type === 'checkbox') {
        return new CheckboxComponent(initialValue);
    }
}

class MyForm extends Component {
    constructor(controls) {
        super();
        this._container = document.createElement('form');
        this._controls = controls.map(controlObj => controlFactory(controlObj));
        this._controls.forEach(control => control.appendTo(this._container));
    }
}

const modal = new Modal(new Card());
modal.open();

const controls = [
    {type: 'text', initialValue: '1234e'},
    {type: 'text', initialValue: '1234e'},
    {type: 'text', initialValue: '1234e'},
    {type: 'text', initialValue: '1234e'},
    {type: 'text', initialValue: '1234e'},
    {type: 'text', initialValue: '1234e'},
    {type: 'checkbox'},
    {type: 'checkbox'},
    {type: 'checkbox'},
    {type: 'checkbox'},
]
const form = new MyForm(controls);
form.appendTo(document.body);
