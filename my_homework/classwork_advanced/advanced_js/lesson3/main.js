// garbare collection
//event queue

// let a = 3;
//
// setTimeout(function d()  {
//     let b = 3;
//     a++;
//     d()
// }, 1000);
// setInterval(надо очищать)

// const a =1;
// const  button = document.querySelector('button');
//
// button.addEventListener('click', () => {
//     console.log('click');
// });
//
// button.onclick = (function () {});

//event loop
// event queue
//call stack

// setTimeout(()=>{
//     alert(1)
// },100);
// setTimeout(()=>{
//     alert(1)
// },100);
// setTimeout(()=>{
//     alert(1)
// },100);
// setTimeout(()=>{
//     alert(1)
// },100);


//heap(onclick)
//event queue
//call stack inASecond
// document.body.addEventListener('click', function onclick() {
//     alert(1);
// });
//
// // setTimeout(function inASecond()  {
// //     co
// //     for( let i = 0; i < 99999999999; i++){
// //
// //     }
// // }, 1000);
//
// const button = document.querySelector('button');
// button.addEventListener('click', (event) => {
//     event.stopPropagation();
//     console.log(1);
//     for( let i = 0; i < 99999999; i++){
//
// }
// console.log(2);
//
// });

//
// const start = Date.now();
// setTimeout(() => console.log(Date.now() - start));
//
// for( let i = 0; i < 99999999; i++){
//
// }

// const button = document.querySelector('button');
// button.addEventListener('click', () => alert(1));
// button.addEventListener('click', () => alert(2));
// button.addEventListener('click', () => alert(3));
// button.addEventListener('click', () => alert(4));

// requestAnimationFrame
// microtask;
// macrotask;

// animaция делается с этим
// Date.now()
// performance.now()
//
// setTimeout(() => console.log('timeout'), 0);
//
// requestAnimationFrame(function animate(){
//     console.log('anim');
//     // requestAnimationFrame(animate);
// });

// setTimeout(() => console.log(timeout), 0)macrotask
//
// Promise.rosolve().then(()=> console.log('promise'))microtask


// try {
//     const a = null;
//     a.prop = 1;
//     console.log('first');
// }
// catch (error) {
//     console.log(error);
// }
// console.log('log');

//
// class NotValidAgeError extends Error{
//     constructor() {
//         super('Not valid');
//     }
// }
// class NegativeAgeError extends Error{
//     constructor() {
//         super("Negative");
// }
// }
//
// function getAge() {
//
//     const  ageInput = prompt('Enter your age: ');
//     let age;
//     try {
//         age = Number(ageInput);
//         if(!age || isNaN(age)){
//             throw new Error('Not valid');
//         }
//         if(age < 0) {
//             throw new Error('Negative age');
//         }
//     }
//
//     return age;
// }
//
// try {
//     const age = getAge();
// }
// catch (error) {
//     fetch('http://mysite.com/logerror?messege');
// }

//================объективно-ориентированное программирование=======================

// 1) инкапсуляция
// 2) наследование
// 3) полиморфизм

// function getAge() {
// const str = askUser();
// const age = parseAge(str);
// handleDeleteTodos();
// }

// const animal = {
//     name: 'Name'
// }
//
// const dog = {
//     ...animal,
//     bark() {
//         console.log('bark,bark');
//     }
// };
// const cat = {
//     ...animal,
//     legs: 4,
// }
//
// function printName(animal){
//     console.log(animal.name);
// }
//
// printName(dog);
// printName(cat);



//фабрика
function Human () {
    return {
        name: '12343',
    }
}

//blurprint шаблон по которому мы создаем обьекты)

//функция конструктор
function Human2 (name) {
    //this = {
    // __proto__: Human2.prototype
    // };

        this.name = name;
        //bad
    this.getName = function () {
return this.name;
    }
    Object.getPrototypeOf();
        // return this;
}

//good
Human2.prototype.getName2 = function () {
return this.name;
}

Human2.prototype.program = function () {
    console.log('programming');
};

const  person = Human();
const  person2 = new Human2('Test');
console.log(person,person2);
const  person3 = new Human2();
const  person4 = new Human2();
const  person5 = new Human2();
const  person6 = new Human2();

// setTimeout(() => {
//     Human2.prototype.newMatod = () =>{}
// }, 1000);

function  Programmer() {
    // this {
//    __proto__: Programmer.prototype
// }
Human2.call(this, name); //типа super
}

Programmer.prototype = Object.create(Human2.prototype);
Programmer.prototype.constructor = Programmer;

// Programmer.prototype = Human2.prototype;
Programmer.prototype.program = function () {
    Human2.prototype.program.call(this);
console.log('programming more');
};

const prog = new Programmer( 'my name');

function create() {
    function f() {
    }
    f.prototype = obj;
    // const newObj = new f;
    return new f;
}


class Human {
    constructor(name) {
        this.name=name;
        this.getName = function () {
        };
        getName2() {

        };
    }
}

class Programmer extends Human {
    constructor(name) {
        // this.age = 3; так не работает
        super(name);//Human2.call(this,name)
        this.age = 3;
    }

    program() {
        console.log('started');
        const result = super.program();
        console.log('finished');
        return result;
    }
}

// Object.prototype.assign = Object.prototype.assign || function () {
//
// }
// (Object.prototype.assign){ Object.prototype.assign || function () {
//
// }}
