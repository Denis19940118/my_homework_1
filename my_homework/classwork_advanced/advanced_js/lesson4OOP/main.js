// =================================OOP=============================to do list на классах
// state - это состояние на донный моммент времени данных
// DRY - dont repeat yourself
// const applicationState = {
//     list: ['item 1', 'item 2', 'abc'],
//     list2: ['item 11', 'item123 2', 'ab234c', 'asdfsgg123'],
//     menuOpened: false,
// };
//
// class Component {
//      // container;
//      constructor() {
//          this.container = this.createContainer();
//      }
//      createContainer() {
//          // throw new Error ('Write createContainer implementation');
//          // return document.createElement('div');
//          // console.warn('Write createContainer implementation');
//          return document.createElement('div');
//      }
//      appendTo(parent) {
//          parent.append(this.container);
//      }
// }
//
// class Application extends Component{
//     constructor(state) {
//         super();
//
//         const list = new List({list : state.list});
//         list.appendTo(this.container);
//
//         const removableList = new RemovableList({list : state.list2});
//         removableList.appendTo(this.container);
//     }
//     createContainer() {
//        const el =  super.createContainer();
//     el.classList.add('app');
//     return el;
//     }
//
//     // createContainer() {
//     //     // return document.createElement('div');
//     //     const el =  document.createElement('div');
//     //     el.classList.add('list-item');
//     //     return el;
//     // }
// }
//
// class List extends Component {
//     constructor({list}) {
//         super();
//         const items = this.createItems(list);
//         items.forEach(item => {
//             item.appendTo(this.container);
//         });
//     }
//     createContainer() {
//         const el = super.createContainer();
//         el.classList.add('list');
//         return el;
//     }
//     createItems(list) {
//         return list.map(data => new ListItem({data}));
//     }
// }
//
//
// class RemovableList extends List {
//     createItems(list) {
//         return list.map(data => new RemovableListItem({data}));
//     }
// }
//
//
// class ListItem  extends Component{
//     constructor({data}) {
//         super();
//         this.container.innerText = data;
//     }
//     createContainer() {
//         // const el =  document.createElement('div');
//         const el =  super.createContainer();
//         el.classList.add('list-item');
//         return el;
//     }
// }
//
// class RemovableListItem extends ListItem{
//     constructor({state}) {
//         super(state);
//         const button = this.createRemoveButton();
//         this.container.append(button);
//     }
//     createRemoveButton() {
//         const button = document.createElement('button');
//         button.innerText = 'remove';
//         button.addEventListener('click' , this.remove.bind(this));
//         return button
//     }
//     remove() {
//         console.log(this);
//     }
//
// }
//
//
//
//
// const app = new Application(applicationState);
// app.appendTo(document.body);

const initialApplicationState = {
    list: ['item 1', 'item 2', 'abc'],
    list2: ['item 11', 'item123 2', 'ab234c', '128743'],
    menuOpened: false
};

class Component {
    // container;
    constructor(state) {
        this.state = state;
        this.container = this.createContainer();
    }

    createContainer() {
        // throw new Error('Write createContainer implementation');
        // console.warn('Write createContainer implementation');
        return document.createElement('div');
    }

    appendTo(parent) {
        parent.append(this.container);
    }
}

class Application extends Component {
    constructor(state) {
        super();

        const list = new List({list: state.list});
        list.appendTo(this.container);
        const onRemove = (data) => {
            // state.list2.
            state = {
                ...state,
                list2: state.list2.filter(item => item !== data)
            };
            console.log('wants to remove')
        };
        const removableList = new RemovableList({list: state.list2, onRemove});
        removableList.appendTo(this.container);
    }

    createContainer() {
        const el = super.createContainer();
        el.classList.add('app');
        return el;
    }
}

class List extends Component {
    constructor(state) {
        super(state);
        const {list} = state;
        const items = this.createItems(state.list);
        items.forEach(item => {
            item.appendTo(this.container);
        });
    }

    createContainer() {
        const el = super.createContainer();
        el.classList.add('list');
        return el;
    }

    createItems(list) {
        return list.map(data => new ListItem({data}));
    }
}

class RemovableList extends List {
    createItems(list) {
        return list.map(data => new RemovableListItem({data, onRemove: this.state.onRemove}));
    }
}

class ListItem extends Component {
    constructor(state) {
        super(state);
        this.container.innerText = state.data;
    }

    createContainer() {
        const el = super.createContainer();
        el.classList.add('list-item');
        return el;
    }
}

class RemovableListItem extends ListItem {
    constructor(state) {
        super(state);
        const button = document.createElement('button');
        button.innerText = 'remove';
        this.container.append(button);
        console.log(state);
    }

    createRemoveButton() {
        const button = document.createElement('button');
        button.innerText = 'remove';
        button.addEventListener('click', this.remove.bind(this));
        return button
    }

    remove() {
        // console.log(this);
        this.container.remove();
        this.state.onRemove(this.state.data);
    }
}


const app = new Application(initialApplicationState);
app.appendTo(document.body);
