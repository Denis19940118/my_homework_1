// DRY - dont repeat yourself

// const initialApplicationState = {
//     list: ['item 1', 'item 2', 'abc'],
//     list2: ['item 11', 'item123 2', 'ab234c', '128743'],
//     menuOpened: false
// };

// class Component {
//
//     static PROP = 123;
//     // container;
//     constructor(props) {
//         this.props = props;
//         this.container = this.createContainer();
//     }
//
//     createContainer() {
//         // throw new Error('Write createContainer implementation');
//         // console.warn('Write createContainer implementation');
//         return document.createElement('div');
//     }
//
//     appendTo(parent) {
//         parent.append(this.container);
//     }
//
//     destroy() {
//         this.container.remove();
//     }
// }
//
// class Application extends Component {
//     constructor(props) {
//         super(props);
//         Component.call(this, props);
// // Component.apply(this, props);
//         const list = new List({list: props.list});
//         list.appendTo(this.container);
//         const onRemove = (data) => {
//             // const newList2 = [...props.list2];
//             // props.list2.splice(props.list2.indexOf(data), 1);
//
//             props = {
//                 ...props,
//                 list2: props.list2.filter(item => item !== data)
//             };
//
//             removableList.destroy();
//             removableList = new RemovableList({list: props.list2, onRemove});
//             removableList.appendTo(this.container);
//         };
//         let removableList = new RemovableList({list: props.list2, onRemove});
//         removableList.appendTo(this.container);
//     }
//
//
//     createContainer() {
//         const el = super.createContainer();
//         el.classList.add('app');
//         return el;
//     }
// }

// class List extends Component {
//     constructor(props) {
//         super(props);
//         const items = this.createItems(props.list);
//         items.forEach(item => {
//             item.appendTo(this.container);
//         });
//     }
//
//     createContainer() {
//         const el = super.createContainer();
//         el.classList.add('list');
//         return el;
//     }
//
//     createItems(list) {
//         return list.map(data => new ListItem({data}));
//     }
// }
//
// class RemovableList extends List {
//     createItems(list) {
//         return list.map(data => new RemovableListItem({data, onRemove: this.props.onRemove}));
//     }
// }
//
// class ListItem extends Component {
//     constructor(props) {
//         super(props);
//         this.container.innerText = props.data;
//     }
//
//     createContainer() {
//         const el = super.createContainer();
//         el.classList.add('list-item');
//         return el;
//     }
// }
//
// class RemovableListItem extends ListItem {
//     constructor(props) {
//         super(props);
//         const button = this.createRemoveButton();
//         this.container.append(button);
//     }
//
//     createRemoveButton() {
//         const button = document.createElement('button');
//         button.innerText = 'remove';
//         button.addEventListener('click', this.remove.bind(this));
//         return button;
//     }
//
//     remove() {
//         // this.container.remove();
//         this.props.onRemove(this.props.data);
//     }
//
// }
//
//
// const app = new Application(initialApplicationState);
// app.appendTo(document.body);

const initialApplicationState = {
    list: ['item 1', 'item 2', 'abc'],
    list2: ['item 11', 'item123 2', 'ab234c', '128743'],
    menuOpened: false
};

function Component(props) {
    this.props = props;
    this.container = this.createContainer();
}

Component.prototype = {
    createContainer() {
        return document.createElement('div')

    },
};

Component.prototype.createContainer = function () {
    return document.createElement('div')
};

Component.prototype.appendTo = function () {
    parent.append(this.container);
};

Component.prototype.destroy = function () {
    this.container.remove();
};

function Application(props) {
    // super(props);
    Component.call(this, props);
// Component.apply(this, props);
    const list = new List({list: props.list});
    list.appendTo(this.container);
    const onRemove = (data) => {
        // const newList2 = [...props.list2];
        // props.list2.splice(props.list2.indexOf(data), 1);

        props = {
            ...props,
            list2: props.list2.filter(item => item !== data)
        };

        removableList.destroy();
        removableList = new RemovableList({list: props.list2, onRemove});
        removableList.appendTo(this.container);
    };
    let removableList = new RemovableList({list: props.list2, onRemove});
    removableList.appendTo(this.container);

}

Application.prototype = Object.create(Component.prototype);
//Component.prototype
/*
*
* {
* appendTo {
* createContainer() {}
* destroy() {}
* }
*
* {
* __proto__:{
* appendTO () {}
* createContainer() {}
* destroy() {}
* }
* }
*
* const aoo = new Application()
* {
* props:...,
* container: ....,
* __proto__: {
* createContainer,
* __proto__:{
*    appendTO () {}
*    createContainer() {}
*    destroy() {}
* }
* }
* }
*
* */


Application.prototype.createContainer = function () {

    const el = Component.prototype.createContainer.call(this);
    el.classList.add('app');
    return el;
};


class List extends Component {
    constructor(props) {
        super(props);
        const items = this.createItems(props.list);
        items.forEach(item => {
            item.appendTo(this.container);
        });
    }

    createContainer() {
        const el = super.createContainer();
        el.classList.add('list');
        return el;
    }

    createItems(list) {
        return list.map(data => new ListItem({data}));
    }
}

class RemovableList extends List {
    createItems(list) {
        return list.map(data => new RemovableListItem({data, onRemove: this.props.onRemove}));
    }
}

class ListItem extends Component {
    constructor(props) {
        super(props);
        this.container.innerText = props.data;
    }

    createContainer() {
        const el = super.createContainer();
        el.classList.add('list-item');
        return el;
    }
}

class RemovableListItem extends ListItem {
    constructor(props) {
        super(props);
        const button = this.createRemoveButton();
        this.container.append(button);
    }

    createRemoveButton() {
        const button = document.createElement('button');
        button.innerText = 'remove';
        button.addEventListener('click', this.remove.bind(this));
        return button;
    }

    remove() {
        // this.container.remove();
        this.props.onRemove(this.props.data);
    }

}


const app = new Application(initialApplicationState);
app.appendTo(document.body);

/*
* 1) class A {} -> function A() {}
*
* 2)class A {
* constructor(data) {
* this.c = data;
* }
* }
* -------
*
*
* function A (data) {
* this.c;
*
* }
*
* 3)
* function A (data) {
* this.c;
* }
*
* class A {
* constructor
*
*
* 5)class A {
* method(){
* super.method(123)
* }
* }
*
* class B extends A{
* }
* -------
*
* function A() {
* A.prototype.method = function(data) {}
* A.prototype.method2 = function() {}
*
*function B() {}
*B.prototype = Object.create(A.prototype);
*
* 6)
* class A {
* method(){
* super.method(123)
* }
* }
*
* class B extends A{
* method() {
* console.log(123);
* }
* method2 {
* super.method(123);
* }
* }
* -------
*
* function A() {
* A.prototype.method = function(data) {}
* A.prototype.method2 = function() {}
*
*function B() {}
*B.prototype = Object.create(A.prototype);
* B.prototype.method = function() {
* console.log(123)
*
* };
*
* B.prototype.method2 = function2 () {
* A.prototype.method.call(this, 123);
*
* }
*
*
* 7)
* class A {
* static SIZE = 100
* constructor(prop = 3){
* this.prop = prop;
* }
* func(){
* };
* func2(data){
* return data
* }
* func3(data, data2) {
* return this.prop + data + data2
* }
* }
* class  B extends A {
* constructor(prop, prop2 = 4 ) {
* super(prop2);
*
* }
* func2() {
*
* const res = super.func2(1)
* return res + 5;
*
* }
* func3(data = 5){
* super.func3(123, data);
* console.log(123);
* return 2;
* }
*
* }
*
*
* */