// class Counter {
//     constructor(initialValue) {
//         this._..
//         this._createUI();
//         this._listenButtons();
//         this.updateValue(initialValue);
//     }
//
//     _createUI() {
//         const container = document.createElement('div');
//         const buttonDecreated = document.createElement('button');
//         buttonDecreated.innerText = '-';
//         const buttonIncreated = document.createElement('button');
//         buttonIncreated.innerText = '+';
//         const input = document.createElement('input');
//         container.append(buttonDecreated);
//         container.append(input);
//         container.append(buttonIncreated);
//         this._input = input;
//         this._container = container;
//         this._buttonIncreated = buttonIncreated;
//         this._buttonDecreated = buttonDecreated;
//     }
//
//     _listenButtons() {
//         this._decrementBound = this.decrement.bind(this);
//         this._buttonDecreated.addEventListener('click', this._decrementBound);
//         this._incrementBound = this.increment.bind(this);
//         this._buttonIncreated.addEventListener('click', this._incrementBound);
//     }
//
//     decrement() {
//         // this._input.value--;
//         // this._input.value = (parseFloat(this._input.value) - 1).toString();
//
//         const newValue = parseFloat(this._input.value) - 1;
//         this._input.value = newValue.toString();
//         if(this._onChangeCallback){
//             this._onChangeCallback(newValue);}
//     }
//
//     increment() {
//         // this._input.value++;
//         // this._input.value = (parseFloat(this._input.value) + 1).toString();
//
//         const newValue = parseFloat(this._input.value) + 1;
//         this._input.value = newValue.toString();
//         if(this._onChangeCallback){
//         this._onChangeCallback(newValue);}
//     }
//
//     updateValue(value) {
//         this._input.value = value;
//     }
//
//     appendTo(parent) {
//         parent.append(this._container)
//     }
//
//     onChange(callback) {
//         this._onChangeCallback = callback;
//     }
//
//     destroy() {
//         this._container.remove();
//         this._buttonDecreated.removeEventListener('click', this._decrementBound);
//         this._buttonIncreated.removeEventListener('click', this._incrementBound);
//     }
// }
//
// // for(let i = 0 ; i< 10; i++){
//     const counter = new Counter(7);
//     counter.appendTo(document.body);
//     counter.onChange((newVal) => console.log(newVal));
//     counter.onChange((newVal) => console.warn(newVal));
// // }
// // const counter = new Counter(7);
// // counter.appendTo(document.body);
// // counter.destroy();
// // counter._listenButtons();


class Counter {
    constructor(initialValue) {
        this._callbacks = [];
        this._createUI();
        this._listenButtons();
        this.updateValue(initialValue);
    }

    _createUI() {
        const container = document.createElement('div');
        const buttonDecrement = document.createElement('button');
        buttonDecrement.innerText = '-';
        const buttonIncrement = document.createElement('button');
        buttonIncrement.innerText = '+';
        const input = document.createElement('input');
        container.append(buttonDecrement);
        container.append(input);
        container.append(buttonIncrement);
        this._input = input;
        this._container = container;
        this._buttonIncrement = buttonIncrement;
        this._buttonDecrement = buttonDecrement;
    }

    _listenButtons() {
        this._decrementBound = this.decrement.bind(this);
        this._buttonDecrement.addEventListener('click', this._decrementBound);
        this._incrementBound = this.increment.bind(this);
        this._buttonIncrement.addEventListener('click', this._incrementBound);
    }

    decrement() {
        // this._input.value--;
        const newValue = parseFloat(this._input.value) - 1;
        this._input.value = newValue.toString();
        this._callbacks.forEach(callback => callback(newValue));
    }

    increment() {
        const newValue = parseFloat(this._input.value) + 1;
        this._input.value = newValue.toString();
        this._callbacks.forEach(callback => callback(newValue));
    }

    updateValue(value) {
        this._input.value = value;
    }

    appendTo(parent) {
        parent.append(this._container);
    }

    onChange(callback) {
        this._callbacks.push(callback);
    }

    destroy() {
        this._container.remove();
        this._buttonDecrement.removeEventListener('click', this._decrementBound);
        this._buttonIncrement.removeEventListener('click', this._incrementBound);
    }
}

// for(let i = 0 ; i < 10; i++) {
const counter = new Counter(7);
counter.appendTo(document.body);
counter.onChange((newVal) => console.log(newVal));
counter.onChange((newVal) => console.warn(newVal));
// }

