// Регулярные выражения!
// . - любой одиночный символ
//     [ ] - любой из них, диапазоны
// $ - конец строки
// ^ - начало строки
// \ - экранирование
// \d - любую цифру
// \D - все что угодно, кроме цифр
// \s - пробелы
// \S - все кроме пробелов
// \w - буква
// \W - все кроме букв
// \b - граница слова
// \B - не границ
//
// Квантификация
// n{4} - искать n подряд 4 раза
// n{4,6} - искать n от 4 до 6
// * от нуля и выше
// + от 1 и выше
//     ? - нуль или 1 раз

// const regex = new RegExp('\\d{3}');
// const regex = /\d{3}/;
//
// let str = '123\'123';
// const regex = /\d{2,3}/;
// const regex = /\d+/g;
// console.log('abc422p73abc'.match(regex));
// console.log('abc422p73abc'.replace('2',''));
// console.log('abc422p73abc'.replace(/2/g,(...args) =>{}));
// console.log('abc422p73abc'.replace(/\d+/g,(...args) =>{
//     console.log(args);
//     return args[0];
//
// }));
// console.log('abc422p73abc'.replace(/(\d)\d*/g,(...args) =>{
//     console.log(args);
//     return args[0];
//
// }));

const regex = /\d+/g;
console.log('a2.9bc422p7,32abc'.replace(/(0|[1-9]\d*)([.,](\d+))?/g, (num, int, _, frac) => {
    if(frac === undefined) {
        return `(${num})`;
    }
    return `(${int}_${frac})`;
}));


// console.log('ab2.9c422p7,32abc'.replace(/^(0|[1-9]\d*)([.,](\d+))?$/g,(num,int,) =>{
//     if(frac === undefined){
//         return num
//     }
//     return `(${int},${frac})`;
//
// }));

// console.log('abc123abc'.match(regex));
//
// console.log('abc123abc'.match(regex));
//
// console.log('abc123abc'.match(regex));
//
// console.log('abc123abc'.match(regex));
//
// console.log('abc123abc'.match(regex));

// const regex = /\d{1,3}/;
// const regex = /a{1,3}/;
// const regex = /1+/;
// console.log('abc111184p73abc'.match(regex));
//
// console.log('abc84p73abc'.match(regex));
// const nums = [{value: 1}, {value: 2}, {value: 3}, {value: 4}, {value: 5}, {value: 6}, {value: 12}];
// const nums = [{value:1},{value:2}, ...];
// nums.map(num => ({value:num}));
// const regexx = /[0123456789]{3}/;
// '123857iryjdjglkdg888skdjf 4668iwi'.match(regexx)
// \d
// +     -> {1,} -повторение от1 и больше
// *     -> {0,} -повторение от0 и больше
// \s - space пробельный символ
// () - оператор группировки(сохранение)
//[154] - либо 1 либо 5 либо 4 (что-то одно из квадратных скобок)
//[^154] - любое кроме одноб пяти и четырех ('154411')
// \w - [a-zA-Z]
// \D === [^\d]
// \W === [^\w]
// . - любой символ
// | - либо (123|12345)
// ? (0,1) либо есть либо нет

// \s*(\d+)\s*
// {value:$1}

// /\w[-\w\d]*="([^"]*)"/
// data-$1='$2'



// 1
// 'abcd' // valid
// 'abccccd' // valid
// 'abd' // valid

// 2
// 'abcde'
// 'abe'
// /^ab(cd)?2$/




// 0,001
// 10.923
// X  00.123
// X  01.123
// 0,328
// 123

// '123'.search()
// '123'.search(/^\d$/);
// console.log(/^\d+/.test('a123')); //false
// console.log(/\d+/.test('a123'));//true
// console.log(/^\d+/.test('123a'));//true
// console.log(/^\d+$/.test('123a'));//false
// console.log(/^\d+$/.test('1a23'));//true
// console.log(/^[A-Z]/.test('1a23'));//true
// console.log(/^(0|[1-9])\d+$/.test('00'));//true
// console.log(/^(0|[1-9]\d*)([.,]\d+)?$/.test('00'));//true

//========validation========
// const numberRegex = /^(0|[1-9]\d*)([.,]\d+)?$/;
// const input = document.querySelector('input');
// input.addEventListener('input', onInput);
// input.style.outline = 'none';
// function  onInput() {
// if (numberRegex.test(input.value)) {
//     input.style.border = '1px solid green';
//     return;
// }
// // else {
//     input.style.border = '1px solid red';
// // }
// }