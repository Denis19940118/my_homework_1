

class Draggable {
    constructor(_element, {distance = 0} = {}) {
        this._element = _element;
        this._distance = distance;
        this._prepareStyles();
        // this._listenStart();
        this.enable();
    }

    enable() {
        this.enabled = true;
        this._onMousedownBound = this._onMousedown.bind(this);
        this._element.addEventListener('mousedown', this._onMousedownBound);
    }

    disable() {
        this.enabled = false;
        this._element.removeEventListener('mousedown', this._onMousedownBound);

    }

    _prepareStyles() {
        this._element.style.position = 'absolute'
    }

    // _listenStart() {
    //     // this._onMousedownBound = this._onMousedown.bind(this);
    //     // this._element.addEventListener('mousedown', this._onMousedownBound);
    // }

    _onMousedown({offsetX, offsetY, pageX, pageY}) {
        this._offsetX = offsetX;
        this._offsetY = offsetY;
        this._startX = pageX;
        this._startY = pageY;
        // console.log('down');
        this._onMousemoveBound = this._onMousemove.bind(this);
        this._onMouseupBound = this._onMouseup.bind(this);
        document.addEventListener('mousemove', this._onMousemoveBound);
        document.addEventListener('mouseup', this._onMouseupBound);
        // document.addEventListener('mouseup', this._onMouseup.bind(this),{once: true});
    }

    _onMousemove({pageX, pageY}) {
        if (!this._isDragging) {
            this._isDragging = Math.hypot(pageX - this._startX, pageY - this._startY) >= this._distance;
        }
        if(!this._isDragging){
        // const isDragging = Math.hypot(pageX - this._startX, pageY - this._startY) >= this._distance;
        // console.log(isDragging);

        const {top,left,bottom,right} = this._element.parentElement.getBoundingClientRect();
        // let y = event.pageY - this._offsetY < top ? top : event.pageY - this._offsetY
        // if (y<top) {
        //     y = top;
        // }

        // const y = Math.min(bottom - this._element.offsetHeight ,
        //     Math.max(event.pageY - this._offsetY,
        //         top
        //         ));
       let  y = pageY - this._offsetY;
        y = Math.max( y, top);
         y =  Math.min( y, bottom - this._element.offsetHeight);

        let  x = pageX - this._offsetX;
        x = Math.max(x, left);
        x = Math.min(x, right - this._element.offsetWidth);
        // let y = Math.max(event.pageY - this._offsetY, top);
        // y = Math.min(y, bottom - this._element.offsetHeight);
        // this._element.style.left = `${event.pageX - 26}px`;
        // this._element.style.top = `${event.pageY - 26}px`;
        // this._element.style.left = `${event.pageX - this._element.offsetWidth / 2}px`;
        // this._element.style.top = `${event.pageY - this._element.offsetHeight / 2}px`;
        // if(isDragging ) {
        this._element.style.left = `${x}px`;
        this._element.style.top = `${y}px`;
        // }
        }
        // else {
        //     this._isDragging = Math.hypot(pageX - this._startX, pageY - this._startY) >= this._distance;
        // }
    }

    _onMouseup(event) {
        this._isDragging = false;
        // console.log('up');
        document.removeEventListener('mousemove', this._onMousemoveBound);
        document.removeEventListener('mouseup', this._onMouseupBound);

    }
}

//
// new Draggable(document.querySelector('.box'));
// document.querySelectorAll('.box').forEach(box => new Draggable(box));
const box = document.querySelector('.box');
const dnd = new Draggable(box, {distance: 10});
const toggleDND = document.querySelector('button');

toggleDND.addEventListener('click', () => {
    if (dnd.enabled) {
        dnd.disable()
    } else {
        dnd.enable();
    }
});