const target = document.querySelector('.target');

target.addEventListener('dragover',onDragOver);
// target.addEventListener('dragenter',onDragEnter);
target.addEventListener('drop',onDrop);

function onDragOver(event) {
    event.preventDefault();
}

function onDragStart(event) {
    // console.log(event);
    event.dataTransfer.setData('text', '.sourse');
}

function onDrop(event) {
    event.preventDefault();

    if(event.dataTransfer.files.length){
        const reader = new FileReader();
        reader.readAsDataURL( event.dataTransfer.files[0]);
        reader.onload = () => {
            const image = document.createElement('img');
            image.style.maxWidth = '80px';
            image.src = reader.result;
            target.append(image);
    };
    // console.log('drop', event.dataTransfer.files);
    // const reader = new FileReader();
    // reader.readAsDataURL( event.dataTransfer.files[0]);
    // reader.onload = () => {
    //     const image = document.createElement('img');
    //     image.style.maxWidth = '80px';
    //     image.src = reader.result;
    //     target.append(image);
        // fetch('mysite.com/upload', {dataTransfer.files})
        //img base64 format for little image
        return;
    }
    const  selector = event.dataTransfer.getData('text');
    console.log('drop', event.dataTransfer.getData('text'));
    target.append(document.querySelector(selector));
}