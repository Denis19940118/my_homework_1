// $.ajax(...).done(onListReceved );
// console.log(data);
//
// function onListReceved (data) {
// render();
// }

// ===========посмотреть функцию=====


// let list1 = document.querySelector('.list-1');
// // console.log(list1);
// // $.getJSON('http://dan-it.herokuapp.com/list1')
// //     .then(res => console.log(res));
// // $.getJSON('http://dan-it.herokuapp.com/list2')
// //     // .then(res => console.log(res));
// //     .done(res => {console.log(res);
// // //     })
// //
// // //[{name: 'Name', age: 10}]

class List {
    // static url = 'http://dan-it.herokuapp.com/list1';
    constructor(selector, dataUrl) {
        this._url = dataUrl;
        this._loading = false;
        this._el = document.querySelector(selector);
        this._initReloadButton();
        this._getData();
    }

    _initReloadButton() {
        const button = document.createElement('button');
        button.type = button;
        button.innerText = 'Reload';
        button.addEventListener('click', this._reload.bind(this));
        this._el.after(button)
    }

    _reload() {
        this._getData();

    }

    _getData() {
        if(this._loading){
            return;
        }
        this._loading = true;
        this._el.classList.add('loading');
        this._el.innerText = '';
        $.getJSON(this._url)
            .done(this._onData.bind(this))
            .always(() =>{
                this._loading = false;
                this._el.classList.remove('loading')
            })
    }

    _onData(users) {
        users.forEach(({name, age}) => {
            const li = document.createElement('li');
            li.innerText = `${name}: ${age}`;
            this._el.append('li');
        }
            // .bind(this)
            )
    }

}

const list1 = new List('.list-1',
    'http://dan-it.herokuapp.com/list1'
    )
const list2 = new List('.list-2',
    'http://dan-it.herokuapp.com/list2'
    )