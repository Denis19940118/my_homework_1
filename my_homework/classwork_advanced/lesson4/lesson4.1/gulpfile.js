const {src,dest} = require('gulp');
const sass = require('gulp-sass');

sass.compiler = require('node-sass');

exports.default = () => src("style.scss")
    .pipe(sass())
    .pipe(dest("./"));

gulp.task('watch', function () {
    gulp.watch('./sass/**/*.scss', gulp.series('sass'));
});