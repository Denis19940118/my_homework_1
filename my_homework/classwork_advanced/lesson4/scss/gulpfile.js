const {src, dest, series, parallel} = require('gulp');
const sass = require('gulp-sass');
sass.compiler = require('node-sass');
const postcss = require('gulp-postcss');
const uncss = require ('postcss-uncss');
const del = require ('del');

// const path = require('path');
//
// let currentPath = path.dirname;
//
// let distFolder = path.resolve(currentPath, './dist');
// path.resolve();

function emptyDist() {
return del('./dist/**');
}

function copyJs() {
    return src('./src/js/main.js')
        .pipe(dest('./dist'))
}

function copyHtml() {
    return src('./src/index.html')
        .pipe(dest('./dist'))
}

function buildScss() {
    return src('./src/scss*.scss')
        // .pipe(sass())
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(postcss([
            uncss ({html: ['./dist/index.html']})
        ]))
        .pipe(dest("./dist"))
}

function copyAssets() {
 return src('./src/assets**')
     .pipe(dest('./dist/assets'))
}

exports.html = copyHtml;
exports.scss = buildScss;
exports.assets = copyAssets;
exports.clear = emptyDist;
exports.js = copyJs;

exports.build = series(
    emptyDist,
    parallel(
        series(copyHtml, buildScss),
        copyAssets,
        copyJs
        ));

exports.default = () => src("style.scss")
.pipe(sass())
.pipe(dest("./"));
