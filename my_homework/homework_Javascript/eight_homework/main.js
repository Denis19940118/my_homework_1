initInput(document.querySelector('div'));

function initInput(root) {
    root.append(Input());
}


function Input() {
    let container = document.createElement('div');
    container.classList.add('containerRevers');
    let items = [];

    const createForm = CreateInput({onCreate});
    let list = ItemList({
        items,
        onTaskRemove,
    });

    function onCreate(name) {
        items = [{name}];
        rerender();
    }

    function onTaskRemove(name) {
        items = items.filter(item => item.name !== name);
        rerender();
    }

    function rerender() {
        const newItemList = ItemList({items, onTaskRemove});
        list.before(newItemList);
        list.remove();
        list = newItemList;
    }
    container.append(createForm);
    container.append(list);

    return container
}


function CreateInput({onCreate, name, onTaskRemove }) {
    let container = document.createElement('div');
    let form = document.createElement('form');
    let input = document.createElement('input');
    let span = document.createElement('span');
    let inputClassList = input.classList;

    inputClassList.add('input');
    inputClassList.add('inputGrey');
    form.classList.add('containerForm');
    span.innerText = 'Текущая цена : ';
    input.before(`Price, $ : `);

    input.addEventListener('focus', onClick);
    input.addEventListener('blur', onClickOff);
    input.placeholder = 'Enter price';
    input.innerText = 'Price';

    function onClick(event) {
        if (event.type === 'focus') {
            inputClassList.remove('inputGreen');
            inputClassList.remove('inputRed');
            inputClassList.add('inputGrey');
            let spanEl = span.querySelector('span');
             spanEl.parentNode.removeChild(spanEl);
                         }
        }


    function onClickOff(event) {
        if (event.type === 'blur') {
            inputClassList.remove('inputGreen');
            inputClassList.remove('inputRed');
            inputClassList.add('inputGrey');
            onCreate(input.value);
        }
    }

    // input.addEventListener('onfocus', (value) => this.value = '');
    // input.addEventListener('onblur', (value) => this.value = 'Enter price');

    container.insertAdjacentElement('afterbegin', form);
    form.insertAdjacentElement('afterbegin', input);
    container.insertAdjacentElement('afterbegin', span);

    return container;
}


function ItemList({items, onTaskRemove}) {
    const container = document.createElement('div');
    container.classList.add('container');

    items.forEach(item => {
        container.append(CreateSpan({...item, onTaskRemove}));
    });

    return container;
}


function CreateSpan({name, onTaskRemove}) {
    let container = document.createElement('div');
    let span = document.createElement('span');
    let spanError = document.createElement('span');

    container.classList.add('container');
    span.classList.add('span');

    let input = document.querySelector('input');
    let spanEl = document.querySelector('span');
    spanEl.append(span);

    if ( name < 0 || !+name && name !== ''){
        input.classList.remove('inputGrey');
        input.classList.remove('inputGreen');
        input.classList.add('inputRed');

        span.innerText = ' не верно';
        spanError.innerText = `Please enter correct price`;
        spanError.classList.add('spanError');
        container.append(spanError);
    } else {
        span.innerHTML = name;
        // input.classList.remove('inputGrey');
        input.classList.remove('inputRed');
        input.classList.add('inputGreen');
    }

    const button = document.createElement('button');
    button.classList.add('buttonSpan');
    button.innerText = 'x';
    span.append(button);

    button.onclick = function () {
        onTaskRemove(span.innerHTML =  name);
        span.style.display = 'none';
        input.value = '';
        input.classList.add('inputGrey');
    };

    return container;
}