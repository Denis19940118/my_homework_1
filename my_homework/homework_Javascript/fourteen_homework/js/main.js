$(document).ready(function () {
    jQuery('.opacity_button').on('click', function () {
        $('.hide-show').slideToggle({
            duration: 800,
            easing: 'linear',
            complete: function () { // callback
                console.log("slideToggle completed");
            },
            queue: false,
        });
    });


});

$(document).ready(function () {
    $('a[data-tar^="link"]').click(function (event) {
        event.preventDefault();
        let element = $($(this).attr('href'));
        console.log(element);
        let lift = $(element).offset().top-30;

        $('html,  body').animate({scrollTop: lift}, 1000);

        return false;
    });

});


let $btnTop = $('.tops-button');
$(window).on('scroll', function () {
    if ($(window).scrollTop() >= 700) {
        $btnTop.fadeIn();
    } else {
        $btnTop.fadeOut();
    }
});

$btnTop.on('click', function () {
    $('html, body').animate({scrollTop: 0}, 1900);
});
