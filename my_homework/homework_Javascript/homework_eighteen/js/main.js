function clonObj(target, obj) {
for (const prop in obj) {
    if ( typeof obj[prop] === 'object' &&  obj[prop] !== null) {
        target[prop] = clonObj({},obj[prop]);
    }
    else {
        target[prop] = obj[prop];
    }
}
return target;
}