let cart = {
    'sisls23' : {
        'name' : 'blabla',
        "count" : 3,
    },
    'pqmry28' : {
        'name' : 'blabla',
        "count" : 3,
    },
};

document.addEventListener('click', (ev) => {// console.log(ev.target.classList);
    if (ev.target.classList.contains('plus')){// console.log(ev.target.dataset.id)
        plusFunction(ev.target.dataset.id);
    }
    if (ev.target.classList.contains('minus')){
        minusFunction(ev.target.dataset.id);
    }
});

// увиличение количества товара
const plusFunction = id => {
    cart[id]['count']++;
    renderCart();
};

// уменьшение количества товара
const minusFunction = id => {
    if (cart[id]['count'] - 1 === 0) {
     deleteFunction(id);
     return true;
    }
        cart[id]['count']--;
        renderCart();
};

// удаление товара
const deleteFunction = id => {
    delete cart[id];
    renderCart();
};

renderCart = () => {
    console.log(cart);
};

renderCart();