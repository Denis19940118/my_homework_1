// let elem = document.body;
//
// alert(elem.nodeType);
//
// alert(elem.firstChild.nodeType);
//
// alert(document.nodeType);

// //подсчет элементов
// for ( let el of document.querySelectorAll('li')){
//     let li = el.firstChild.data;
//
//     li = li.trim();
//
//     let count = el.getElementsByTagName('li').length
//
//     alert(li + ':' + count);
// }
// console.log('---')
// let ul = document.querySelectorAll('li');
// for (let i = 0; i < ul.length; i++) {
//     console.log( 'Название <li> элемента: "' +
//     (ul[i].textContent ? ul[i].firstChild.textContent.trim() :ul[i].data.trim())
//     + '", номер <li> элемента: '
//     + i)
// }


// alert(document.body.lastChild.nodeType);

// let body = document.body;
//
// body.innerHTML = "<!--" + body.tagName + "-->";
//
// alert( body.firstChild.data ); // BODY

// Объектом какого класса является document?
//
//     Какое место он занимает в DOM-иерархии?
//
//     Наследует ли он от Node или от Element, или может от HTMLElement?
//
// alert(document);
//
// alert(document.constructor.name);
// alert(HTMLDocument.prototype.constructor === HTMLDocument); // true
// alert(HTMLDocument.prototype.constructor.name); // HTMLDocument
// alert(HTMLDocument.prototype.__proto__.constructor.name); // Document
// alert(HTMLDocument.prototype.__proto__.__proto__.constructor.name); // Node
// console.dir(document);



function showNotification({top = 0, rigth = 0 , className, html}){
let div = document.createElement('div');
div.className = 'notification';

if(className){
    div.classList.add(className)
}

div.style.top = top + 'px';
div.style.right = rigth + 'px';

div.innerHTML = html;
document.body.append(div);

setTimeout(() => div.remove(), 1500);

}

//test it
let i = 10 ;
setInterval(() => {
showNotification({
    top : 10,
    rigth : 10,
    html: '' + i--,
    className: 'welcome',
    })
 }, 2000);