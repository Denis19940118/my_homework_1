document.onwheel = function (event) {
    console.log(event);
    if (event.deltaY > 0) {
        document.getElementById('line').innerText = 'вниз';
    } else {
        document.getElementById('line').innerText = 'вверх';
    }

    let speed = event.deltaY;
    speed = Math.abs(speed);
    if (speed < 100) {
        document.getElementById('speed').innerText = 'низкая';
    } else if (speed < 150) {
        document.getElementById('speed').innerText = 'средняя';
    } else if (speed < 250) {
        document.getElementById('speed').innerText = 'высокая';
    } else {
        document.getElementById('speed').innerText = 'очень высокая';
    }
};

let left = 290;
document.getElementById('test').onwheel = function (event) {

    let line = event.deltaY;
    left = left + line;
    document.getElementById('test2').style.left = left + 'px';
    return false;
};

