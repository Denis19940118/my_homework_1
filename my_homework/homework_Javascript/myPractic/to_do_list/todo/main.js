const todoForm = document.getElementById('form');
const container = document.getElementById('todos');
const deleteBtn = document.getElementById('delete');

const handleFormSubmit = form => {
  const input = form[0];
  const name = input.value;
  input.value = '';
  createTodo(name, container);
};


const handleDeleteTodos = container => {
  [...container.children].forEach(todo => todo.remove())
};

const handleTodoComplete = todo => {
  const text = todo.querySelector('p');
  text.classList.toggle('todo-completed');
};

const createTodo = (data, container) => {
  const todo = document.createElement('div');
  todo.classList.add('todo');

  const completeCheckbox = document.createElement('input');
  const todoName = document.createElement('p');
  completeCheckbox.type = 'checkbox';
  todoName.innerText = data;

  completeCheckbox.addEventListener('change', () => {
    handleTodoComplete(todo)
  });

  todo.append(completeCheckbox, todoName);

  container.append(todo)
};

todoForm.addEventListener('submit', (e) => {
  e.preventDefault();
  handleFormSubmit(e.target);
});

deleteBtn.addEventListener('click',() => handleDeleteTodos(container));
