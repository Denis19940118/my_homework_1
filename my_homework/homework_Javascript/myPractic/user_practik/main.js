// let user = {
//     name: "User name",
//     'last name': "User last name",
//     age: 26,
//     working: 'yes',
//     experience : true,
// };
//  console.log(user);

// const student = {
//     name: "Григорий",
//     "last name": "Сковорода",
//     laziness: 4,
//     trick: 5
// };
//
// console.log(student);
//
//
// const product = {
//     name: "Ноутбук Acer",
//     "full name": "Ноутбук Acer Swift 3 SF314-52 (NX.GNUEU.038)",
//     price: 19048.5,
//     availability: false,
//     "additional gift": null
// }
//
// console.log(product)
//
// let user = {
//     name: "Влад",
//     "second name": "Дракула",
//     age: 400,
//     marriage: false
// };

// console.log(user.name); // Выведет "Влад"
// console.log(user["second name"]); // Выведет "Дракула"
// console.log(user.age); // Выведет "400"
// console.log(user["marriage"]); // Выведет "false"

// user.age = 401; // меняем возраст
//
// user["second name"] = "Цепеш"; // если в имени свойства несколько слов, разделенных пробелом - после
// // имени объекта не ставим точку, а пишем квадратные скобки, внутри которых - имя свойства в кавычках
//
// user.marriage = "под вопросом"; // меняем значение и тип данных, хранящихся в свойстве marriage
//
// console.log(user);


// const student = {
//     name: "Григорий",
//     "last name": "Сковорода",
//     laziness: 4,
//     trick: 5
// };
//
// console.log(student);
//
// // if(3 <= student.laziness <= 5 || student.trick <= 4){
// if(student.laziness <= 3 && student.laziness <= 5 && student.trick <= 4){
//     console.log( 'Student ' + student.name + ' ' + student["last name"] + ' отправлен на пересдачу')
// }
//
// измените коэффициент лени на 6, а коэффициент хитрости - на 3;
// если коэффициент лени больше
// или равен 5, а коэффициент хитрости меньше 4, вывести в консоль сообщение
// "Cтудент имяСтудента фамилияСтудента передан в военкомат".

const student = {
    name: "Григорий",
    "last name": "Сковорода",
    laziness: 4,
    trick: 5
};

student.laziness = 6;
student.trick = 3;
 if (student.laziness >= 5 && student.trick <= 4){
     console.log("Cтудент " + student["last name"] + " " + student.name + " передан в военкомат")
}