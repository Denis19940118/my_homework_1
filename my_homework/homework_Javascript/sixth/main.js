
function filterBy(arr, type) {
    return arr.filter((el) => typeof el !== type) ;
}
console.log(filterBy(['hello', 'world', 23, '23', null, false],'string'));
console.log(filterBy(['hello', 'world', 23, '23', null, false],'number'));
console.log(filterBy(['hello', 'world', 23, '23', null, false],'object'));
console.log(filterBy(['hello', 'world', 23, '23', null, false],'boolean'));